﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace AtomFormUI.UControls {

    public class CircleButton : Button {
        public CircleButton() {
            InitializeComponent(); 
        }

        Rectangle rect;
        protected void InitializeComponent() {
            SetStyle(ControlStyles.Selectable, true); //可获取焦点
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);// 忽略窗口消息 减少闪烁
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true); //双缓冲区
            SetStyle(ControlStyles.UserPaint, true); //控件由用户自己绘制
            SetStyle(ControlStyles.ResizeRedraw, true); // 调整大小时重新绘制
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);// 支持透明背景
            rect = this.ClientRectangle;
            Font = new Font("微软雅黑", 12F);
            FlatStyle = FlatStyle.Flat;// 去掉点击之后的背景
            FlatAppearance.BorderSize = 0;
            FlatAppearance.MouseDownBackColor = Color.Transparent;// 去掉点击之后的聚焦线
            FlatAppearance.MouseOverBackColor = Color.Transparent; 
        } 

        [Description("自动调整大小")]
        public override bool AutoSize { get; set; } = true; 

        protected override void OnSizeChanged(EventArgs e) {
            base.OnSizeChanged(e);
            rect = this.ClientRectangle;
            this.Region = new Region(rect);
            //处理边框边线
            rect.Width -= 1;
            rect.Height -= 1;
        }

        private Color bgColor = Color.Gray;
        [Description("按钮背景色"), DefaultValue(typeof(Color), "Gray")]
        public Color BgColor {
            get { return bgColor; }
            set {
                bgColor = value;
                Invalidate();
            }
        }

        private Color bgColor2 = Color.Transparent;
        [Description("按钮背景色2"), DefaultValue(typeof(Color), "Transparent")]
        public Color BgColor2 {
            get { return bgColor2; }
            set {
                bgColor2 = value;
                Invalidate();
            }
        }

        private Color borderColor = Color.Gray;
        [Description("边框颜色"), DefaultValue(typeof(Color), "Gray")]
        public Color BorderColor {
            get { return borderColor; }
            set {
                borderColor = value;
                Invalidate();
            }
        }
        private int borderWidth = 0;
        [Description("边框粗细"), DefaultValue(typeof(int), "0")]
        public int BorderWidth {
            get { return borderWidth; }
            set {
                borderWidth = value;
                Invalidate();
            }
        }

        private Color selectedColor = Color.Red;
        [Description("选中后的文字颜色"), DefaultValue(typeof(Color), "Red")]
        public Color SelectedColor {
            get { return selectedColor; }
            set {
                selectedColor = value;
                Invalidate();
            }
        }

        private int radius = 5;
        [Description("圆角半径"), Category("Layout"), Browsable(true)]
        public int Radius {
            get { return radius; }
            set {
                radius = value;
                Invalidate();
            }
        }
        private LinearGradientMode gradientMode = LinearGradientMode.Vertical;
        [Description("渐变方式")]
        public LinearGradientMode GradientMode {
            get { return gradientMode; }
            set {
                gradientMode = value;
                Invalidate();
            }
        }
        /// <summary>
        /// 点击的时候是否显示焦距框 点击有虚线
        /// </summary> 
        protected override bool ShowFocusCues { get { return false; } }
        

        bool isMouseDown = false;

        protected override void OnGotFocus(EventArgs e) {
            //base.OnGotFocus(e);
            isMouseDown = true;
        }

        protected override void OnLostFocus(EventArgs e) {
            //base.OnLostFocus(e);
            isMouseDown = false;
        }


        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = SmoothingMode.HighQuality;//高质量呈现
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;//高质量呈现
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;//高质量呈现
            graphics.CompositingQuality =  CompositingQuality.HighQuality;//高质量呈现
            graphics.InterpolationMode =   InterpolationMode.HighQualityBilinear;//高质量呈现
            Rectangle rect1;//
            GraphicsPath path = new GraphicsPath();
            GraphicsPath path2 = new GraphicsPath();
            if (radius > 0) {
                //有圆角
                path = PaintUtils.GetRoundRactangle(rect, radius);//圆角边框路径
                if (borderWidth > 0) {
                    //有边框
                    graphics.FillPath(new SolidBrush(borderColor), path);
                    //内部背景区域
                    rect1 = new Rectangle(rect.X + borderWidth, rect.Y + borderWidth,
                        rect.Width - 2 * borderWidth, rect.Height - 2 * borderWidth);
                    path2 = PaintUtils.GetRoundRactangle(rect1, radius);//圆角边框路径
                } else {
                    //无边框
                    path2 = path;
                    rect1 = rect;
                }
                //填充背景
                if(bgColor2 != Color.Transparent) {
                    //渐变填充
                    LinearGradientBrush gradientBrush = new LinearGradientBrush(rect1, bgColor, bgColor2, gradientMode);
                    graphics.FillPath(gradientBrush, path2);
                } else {
                    //纯色填充
                    Brush brush = new SolidBrush(bgColor);
                    graphics.FillPath(brush, path2);
                }

            } else {

                if (borderWidth > 0) {
                    //有边框
                    graphics.FillRectangle(new SolidBrush(borderColor), rect);
                    //内部背景区域
                    rect1 = new Rectangle(rect.X + borderWidth, rect.Y + borderWidth,
                        rect.Width - 2 * borderWidth, rect.Height - 2 * borderWidth);
                } else {
                    //无边框
                    rect1 = rect;
                }
                //填充背景
                if (bgColor2 != Color.Transparent) {
                    //渐变填充
                    LinearGradientBrush gradientBrush = new LinearGradientBrush(rect1, bgColor, bgColor2, gradientMode);
                    graphics.FillRectangle(gradientBrush, rect1);
                } else {
                    //纯色填充
                    Brush brush = new SolidBrush(bgColor);
                    graphics.FillRectangle(brush, rect1);
                }

            }
            //绘制文本
            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Center;
            format.Alignment = StringAlignment.Center;
            Rectangle rect2 = new Rectangle(2, 2, rect.Width - 4, rect.Height - 4);
            SolidBrush solidBrush = new SolidBrush(this.ForeColor);
            if (this.isMouseDown) {
                solidBrush = new SolidBrush(this.selectedColor) ;

            }
            graphics.DrawString(Text, this.Font, solidBrush, rect2, format);


        }



    }
}
