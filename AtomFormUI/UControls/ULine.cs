﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtomFormUI.UControls {
    public class ULine : UserControl {

        Rectangle rect;
        protected void InitializeComponent() {
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);// 忽略窗口消息 减少闪烁
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true); //双缓冲区
            SetStyle(ControlStyles.UserPaint, true); //控件由用户自己绘制
            SetStyle(ControlStyles.ResizeRedraw, true); // 调整大小时重新绘制
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);// 支持透明背景
            rect = this.ClientRectangle;
            BackColor = Color.Transparent;
            Size = new Size(100, 10);
        }

        protected override void OnSizeChanged(EventArgs e) {
            base.OnSizeChanged(e);
            rect = this.ClientRectangle;
        }

        private Boolean isHorizontal = true;

        [Description("是否水平线")]
        public Boolean IsHorizontal {
            get { return isHorizontal; }
            set {
                bool oldHorizontal = isHorizontal;

                isHorizontal = value;
                if (oldHorizontal != isHorizontal) {
                    Size = new Size(rect.Height, rect.Width);
                }

                Invalidate();
            }
        }

        private int lineWidth = 1;

        [Description("线宽")]
        public int LineWidth {
            get { return lineWidth; }
            set {
                lineWidth = value;
                Invalidate();
            }
        }



        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            if (isHorizontal) {
                Point start = new Point(1, Height / 2 );
                Point end = new Point(Width - 2, Height / 2 );
                Brush brush = new SolidBrush(ForeColor);
                Pen pen = new Pen(brush, lineWidth);
                graphics.DrawLine(pen, start, end);
            } else {
                Point start = new Point(Width / 2, 1);
                Point end = new Point(Width / 2, Height - 2);
                Brush brush = new SolidBrush(ForeColor);
                Pen pen = new Pen(brush, lineWidth);
                graphics.DrawLine(pen, start, end);
            }


        }



    }
}
