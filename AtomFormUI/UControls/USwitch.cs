﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace AtomFormUI.UControls {

    public class USwitch : UserControl
    {
        public USwitch() {
            InitializeComponent();
        }


        Rectangle rect;
        protected void InitializeComponent() {
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);// 忽略窗口消息 减少闪烁
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true); //双缓冲区
            SetStyle(ControlStyles.UserPaint, true); //控件由用户自己绘制
            SetStyle(ControlStyles.ResizeRedraw, true); // 调整大小时重新绘制
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);// 支持透明背景
            rect = this.ClientRectangle;
            BackColor = Color.Transparent;
            MouseDown += USwitch_MouseDown;
            Size = new Size(50, 25);
            Font = new Font("微软雅黑", 12F);
        }

        private void USwitch_MouseDown(object sender, MouseEventArgs e) {

            this.Checked = !this.Checked;
        }

        private Color trueColor = Color.Orange;

        [Description("选中颜色")]
        public Color TrueColor {
            get { return trueColor; }
            set {
                trueColor = value;
                Invalidate();
            }
        }
        private Color falseColor = Color.Gray;

        [Description("选中颜色")]
        public Color FalseColor {
            get { return falseColor; }
            set {
                falseColor = value;
                Invalidate();
            }
        }

        private String[] stateTexts;
        [Description("状态文本")]
        public String[] StateTexts {
            get { return stateTexts; }
            set {
                stateTexts = value;
                Invalidate();
            }
        }

        private bool state = false;
        [Description("选中颜色")]
        public Boolean Checked {
            get { return state; }
            set {
                state = value;
                Invalidate();
            }
        }


        protected override void OnSizeChanged(EventArgs e) {
            base.OnSizeChanged(e);
            rect = this.ClientRectangle;
            this.Region = new Region(rect);
        }

        public override string Text { get => String.Empty; }

        private Color borderColor = Color.Gray;
        [Description("边框颜色"), DefaultValue(typeof(Color), "Gray")]
        public Color BorderColor {
            get { return borderColor; }
            set {
                borderColor = value;
                Invalidate();
            }
        }
        private int borderWidth = 0;
        [Description("边框粗细"), DefaultValue(typeof(int), "0")]
        public int BorderWidth {
            get { return borderWidth; }
            set {
                borderWidth = value;
                Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = SmoothingMode.HighQuality;//高质量呈现
            Color bgColor = state ? trueColor : falseColor;
            GraphicsPath path = new GraphicsPath();
            //上边线
            path.AddLine(new Point(Height / 2, 1), new Point(Width - Height / 2, 1));
            //右边圆弧
            path.AddArc(new Rectangle(Width - Height - 1, 1, Height - 2, Height - 2), -90, 180);
            //下边线
            path.AddLine(new Point(Width - Height / 2, Height - 1), new Point(Height / 2, Height - 1));
            //左边圆弧
            path.AddArc(new Rectangle(1, 1, Height - 2, Height - 2), 90, 180);
            //无边框
            graphics.FillPath(new SolidBrush(bgColor), path); 
            String text = String.Empty;
            //状态文本
            if (stateTexts != null && stateTexts.Length == 2) {
                if (state) {
                    text = stateTexts[0];
                } else {
                    text = stateTexts[1];
                }
            }
            //跟据状态绘制
            if (state) {
                // 开   右边圆  左文本
                graphics.FillEllipse(Brushes.White, new Rectangle(Width - Height - 1 + 2, 1 + 2,
               Height - 2 - 4, Height - 2 - 4));

                if (!String.IsNullOrEmpty(text)) {
                    SizeF fsize = graphics.MeasureString(text.Replace(' ', 'A'), Font);
                    float posY = (Height - fsize.Height) / 2 + 2F;
                    graphics.DrawString(text, Font, Brushes.White, new PointF((Height - 2 - 4) / 2, posY));
                }
            } else {
                // 关   左边圆  右文本
                graphics.FillEllipse(Brushes.White, new Rectangle(1 + 2, 1 + 2, Height - 2 - 4, Height - 2 - 4));

                if (!String.IsNullOrEmpty(text)) {
                    SizeF fsize = graphics.MeasureString(text.Replace(' ', 'A'), Font);
                    float posY = (Height - fsize.Height) / 2 + 2F;
                    graphics.DrawString(text, Font, Brushes.White, new PointF(Width - 2 - Height / 2 - fsize.Width + 4, posY));
                }
            }


        }



    }
}
