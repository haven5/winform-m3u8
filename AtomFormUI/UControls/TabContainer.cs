﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomFormUI.UControls {
    public partial class TabContainer : UserControl {

        private Rectangle rect;


        Point startPoint = Point.Empty;
        [Description("导航点击事件")]
        public event Action<Button, int> TabClick = null;

        public TabContainer() {
            SetStyle(ControlStyles.Selectable, true); //可获取焦点
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);// 忽略窗口消息 减少闪烁
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true); //双缓冲区
            SetStyle(ControlStyles.UserPaint, true); //控件由用户自己绘制
            SetStyle(ControlStyles.ResizeRedraw, true); // 调整大小时重新绘制
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);// 支持透明背景
            rect = this.ClientRectangle;
            InitializeComponent();

            this.MouseDown += (sender, e) => {
                startPoint = e.Location;
            };
            this.MouseMove += (sender, e) => {
                if (startPoint.IsEmpty) {
                    return;
                }
                int x = ParentForm.Location.X - startPoint.X + e.Location.X;
                int y = ParentForm.Location.Y - startPoint.Y + e.Location.Y;
                ParentForm.Location = new Point(x, y);

            };

            this.MouseUp += (sender, e) => {
                startPoint = Point.Empty;
            };

        }


        protected override void OnSizeChanged(EventArgs e) {
            base.OnSizeChanged(e);
            rect = this.ClientRectangle;
            //CreateNav();
            Invalidate();
            //ResizeClient();
        }

        public string Title {
            get => base.Text; set {
                base.Text = value;
                Invalidate();
            }
        }

        private void ResizeClient() {
            int y = rect.Height - pmenu.Location.Y - 10;
            int width = rect.Width - pmenu.Width - 20;
            pmenu.Height = y;
            pmain.Height = y;
            pmain.Width = width;
            int btnWidth = btnMin.Width + 2;
            //重排按钮
            btnMin.Location = new Point(rect.Width - (btnWidth * 3), 5);
            btnMax.Location = new Point(rect.Width - (btnWidth * 2), 5);
            btnClose.Location = new Point(rect.Width - (btnWidth * 1), 5);

        }

        private void btnMin_Click(object sender, EventArgs e) {
            this.ParentForm.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e) {
            if (ParentForm.WindowState == FormWindowState.Maximized) {
                ParentForm.WindowState = FormWindowState.Normal;
                btnMax.Text = "1";
            } else {
                btnMax.Text = "2";
                ParentForm.WindowState = FormWindowState.Maximized;
            }
            CreateNav();
        }

        private void btnClose_Click(object sender, EventArgs e) {
            this.ParentForm.Close();
        }


        private int titleFontSize = 14;
        [Description("标题字体大小")]
        public int TitleFontSize {
            get { return titleFontSize; }
            set { titleFontSize = value; }
        }

        private void FrmTabs_Paint(object sender, PaintEventArgs e) {
            Graphics graphics = e.Graphics;
            if (rect.Height == 0) {
                return;
            }
            LinearGradientBrush linearGradientBrush = new LinearGradientBrush(rect, Color.SkyBlue, Color.Purple, LinearGradientMode.Vertical);
            graphics.FillRectangle(linearGradientBrush, rect);
            DrawTitle(graphics);
        }

        private void DrawTitle(Graphics graphics) {
            if (String.IsNullOrEmpty(Text)) {
                return;
            }
            //绘制标题
            Brush brush = new SolidBrush(ForeColor);
            Font font = new Font(Font.FontFamily, titleFontSize);
            graphics.DrawString(Text, font, brush, new PointF(10, 10));
        }

        private Color navNominalColor = Color.Orange;
        [Description("菜单默认颜色")]
        public Color NavNominalColor {
            get { return navNominalColor; }
            set { navNominalColor = value; }
        }

        private Color navActiveColor = Color.Red;
        [Description("菜单选中颜色")]
        public Color NavActiveColor {
            get { return navActiveColor; }
            set { navActiveColor = value; }
        }

        private void CreateNav() {
            pmenu.Controls.Clear();
            if (listNav == null) {
                return;
            }
            int index = 0;
            int width = pmenu.Width;
            int totalHeight = listNav.Length * (45 + 2);
            if (totalHeight >= pmenu.Height) {
                width -= 22;//留出滚动条宽度
            } else {
                width -= 4;
            }

            foreach (String item in listNav) {
                Button button = new Button();
                button.Text = item;
                button.Width = width;
                button.Height = 45;
                button.Tag = index;
                button.FlatStyle = FlatStyle.Flat;
                button.FlatAppearance.BorderSize = 0;
                button.BackColor = navNominalColor;
                int y = index++ * (button.Height + 2);
                button.Location = new Point(2, y);
                pmenu.Controls.Add(button);
                button.Click += Button_Click;
            }
        }

        private void Button_Click(object sender, EventArgs e) {
            Button button = sender as Button;
            foreach (Control item in pmenu.Controls) {
                if (item == sender) {
                    item.BackColor = NavActiveColor;
                } else {
                    item.BackColor = navNominalColor;
                }
            }
            int index = (int)button.Tag;
            TabClick?.Invoke(button, index);
        }

        private String[] listNav;
        [Description("导航菜单")]
        public String[] ListNav {
            get { return listNav; }
            set {
                listNav = value;
                CreateNav();
            }
        }


        #region 1.0 设置显示的视同 +void SetView(UserControl control)
        /// <summary>
        /// 设置显示的视同
        /// </summary>
        /// <param name="control"></param>
        public void SetView(UserControl control) {
            pmain.Controls.Clear();
            if (control == null) {
                return;
            }
            control.Dock = DockStyle.Fill;
            pmain.Controls.Add(control);
        }
        #endregion

    }
}
