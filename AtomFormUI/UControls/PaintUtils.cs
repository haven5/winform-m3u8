﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace AtomFormUI.UControls {
    public class PaintUtils {



        #region 1.0 根据矩形的半径计算出绘制的轨迹对象 + GraphicsPath GetRoundRactangle(Rectangle rectangle, int r) 
        /// <summary>
        /// 根据矩形的半径计算出绘制的轨迹对象
        /// </summary>
        /// <param name="rectangle"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public static GraphicsPath GetRoundRactangle(Rectangle rectangle, int r) {
            GraphicsPath path = new GraphicsPath();
            int l = 2 * r;
            if (l > rectangle.Height) {
                l = rectangle.Height;
            }
            //上边直线
            path.AddLine(new Point(rectangle.X + r, rectangle.Y), new Point(rectangle.Right - r, rectangle.Y));
            //右上角圆弧
            path.AddArc(new Rectangle(rectangle.Right - l, rectangle.Y, l, l), 270F, 90F);

            //右边竖线
            if (l < rectangle.Height) {

                path.AddLine(new Point(rectangle.Right, rectangle.Y + r), new Point(rectangle.Right, rectangle.Bottom - r));
            }
            //右下角圆弧
            path.AddArc(new Rectangle(rectangle.Right - l, rectangle.Bottom - l, l, l), 0F, 90F);

            //下边直线
            path.AddLine(new Point(rectangle.Right - r, rectangle.Bottom), new Point(rectangle.X + r, rectangle.Bottom));
            //左下角圆弧
            path.AddArc(new Rectangle(rectangle.X, rectangle.Bottom - l, l, l), 90F, 90F);

            //左边竖线
            if (l < rectangle.Height) {
                path.AddLine(new Point(rectangle.X, rectangle.Bottom - r), new Point(rectangle.X, rectangle.Y + r));
            }
            //左上角圆弧
            path.AddArc(new Rectangle(rectangle.X, rectangle.Y, l, l), 180F, 90F);
            return path;
        }
        #endregion

        [DllImport("gdi32.dll")]
        public static extern int CreateRoundRectRgn(int x1, int y1, int x2, int y2, int x3, int y3);

        [DllImport("user32.dll")]
        public static extern int SetWindowRgn(IntPtr hwnd, int hRgn, Boolean bRedraw);

        [DllImport("gdi32.dll", EntryPoint = "DeleteObject", CharSet = CharSet.Ansi)]
        public static extern int DeleteObject(int hObject);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MOVE = 0xF010;
        public const int HTCAPTION = 0x0002;


        #region 2.0 设置窗体圆角 +static void SetFormRadius(Form form, int radius)
        /// <summary>
        /// 设置窗体圆角
        /// </summary>
        /// <param name="form"></param>
        /// <param name="radius"></param>
        public static void SetFormRadius(Form form, int radius) {
            int hRgn = 0;
            hRgn = CreateRoundRectRgn(0, 0, form.Width + 1, form.Height + 1, radius, radius);
            SetWindowRgn(form.Handle, hRgn, true);
            DeleteObject(hRgn);
        } 
        #endregion

    }
}
