﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace AtomFormUI.UControls {

    public class UCircle : Label {
        public UCircle() {
            InitializeComponent(); 
        }

        Rectangle rect;
        protected void InitializeComponent() {
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);// 忽略窗口消息 减少闪烁
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true); //双缓冲区
            SetStyle(ControlStyles.UserPaint, true); //控件由用户自己绘制
            SetStyle(ControlStyles.ResizeRedraw, true); // 调整大小时重新绘制
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);// 支持透明背景
            rect = this.ClientRectangle;
            FlatStyle = FlatStyle.Flat;// 去掉点击之后的背景
            BackColor = Color.Transparent;
            Size = new Size(30, 30);
        } 

        [Description("自动调整大小")]
        public override bool AutoSize  => false; 

        protected override void OnSizeChanged(EventArgs e) {
            base.OnSizeChanged(e);
            this.Width = this.Height;
            rect = this.ClientRectangle;
            this.Region = new Region(rect);
        }

        public override string Text { get => String.Empty; }

        private Color borderColor = Color.Gray;
        [Description("边框颜色"), DefaultValue(typeof(Color), "Gray")]
        public Color BorderColor {
            get { return borderColor; }
            set {
                borderColor = value;
                Invalidate();
            }
        }
        private int borderWidth = 0;
        [Description("边框粗细"), DefaultValue(typeof(int), "0")]
        public int BorderWidth {
            get { return borderWidth; }
            set {
                borderWidth = value;
                Invalidate();
            }
        }
          
        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = SmoothingMode.HighQuality;//高质量呈现
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;//高质量呈现
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;//高质量呈现
            graphics.CompositingQuality =  CompositingQuality.HighQuality;//高质量呈现
            graphics.InterpolationMode =   InterpolationMode.HighQualityBilinear;//高质量呈现

            Rectangle rect1 = new Rectangle(rect.X + 1, rect.Y + 1, rect.Width - 2, rect.Height - 2);
             
            if (borderWidth > 0) {
                //有边框
                graphics.FillEllipse(new SolidBrush(borderColor), rect1);
                //内部背景区域
                Rectangle rectCenter = new Rectangle( rect1.X + borderWidth, rect1.Y + borderWidth,
                    rect1.Width - 2 * borderWidth, rect1.Height - 2 * borderWidth);
                graphics.FillEllipse(new SolidBrush(ForeColor), rectCenter);

            } else {
                //无边框
                graphics.FillEllipse(new SolidBrush(ForeColor), rect1); 
            }  
        }



    }
}
