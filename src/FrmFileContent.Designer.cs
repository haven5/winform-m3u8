﻿
namespace WinFormTsService {
    partial class FrmFileContent {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFileContent));
            this.label1 = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.RichTextBox();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.tsbtnClearAll = new System.Windows.Forms.ToolStripButton();
            this.tsbtnShowFile = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "地址：";
            // 
            // txtMessage
            // 
            this.txtMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMessage.Location = new System.Drawing.Point(2, 102);
            this.txtMessage.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(627, 326);
            this.txtMessage.TabIndex = 1;
            this.txtMessage.Text = "";
            // 
            // txtPath
            // 
            this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPath.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.txtPath.Location = new System.Drawing.Point(57, 61);
            this.txtPath.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(510, 25);
            this.txtPath.TabIndex = 2;
            // 
            // tsbtnClearAll
            // 
            this.tsbtnClearAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnClearAll.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnClearAll.Image")));
            this.tsbtnClearAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnClearAll.Name = "tsbtnClearAll";
            this.tsbtnClearAll.Size = new System.Drawing.Size(36, 22);
            this.tsbtnClearAll.Text = "清空";
            this.tsbtnClearAll.Click += new System.EventHandler(this.tsbtnClearAll_Click);
            // 
            // tsbtnShowFile
            // 
            this.tsbtnShowFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnShowFile.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnShowFile.Image")));
            this.tsbtnShowFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnShowFile.Name = "tsbtnShowFile";
            this.tsbtnShowFile.Size = new System.Drawing.Size(60, 22);
            this.tsbtnShowFile.Text = "显示文件";
            this.tsbtnShowFile.Click += new System.EventHandler(this.tsbtnShowFile_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnClearAll,
            this.tsbtnShowFile});
            this.toolStrip1.Location = new System.Drawing.Point(0, 32);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(631, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // FrmFileContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 431);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximumSize = new System.Drawing.Size(1536, 941);
            this.Name = "FrmFileContent";
            this.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.Text = "文件内容查看器";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtMessage;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.ToolStripButton tsbtnClearAll;
        private System.Windows.Forms.ToolStripButton tsbtnShowFile;
        private System.Windows.Forms.ToolStrip toolStrip1;
    }
}