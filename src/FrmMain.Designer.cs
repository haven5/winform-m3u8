﻿
namespace WinFormTsService {
    partial class FrmMain {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lvMain = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvFileItem = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnFrmAddMultiple = new System.Windows.Forms.ToolStripButton();
            this.btnStartAll = new System.Windows.Forms.ToolStripButton();
            this.btnStopAll = new System.Windows.Forms.ToolStripButton();
            this.btnRemoveAll = new System.Windows.Forms.ToolStripButton();
            this.btnOpenFolder = new System.Windows.Forms.ToolStripButton();
            this.btnDownloadSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.btnStartDownload = new System.Windows.Forms.ToolStripButton();
            this.tsbtnSearch = new System.Windows.Forms.ToolStripButton();
            this.tsbtnReloadTs = new System.Windows.Forms.ToolStripButton();
            this.tsbtnFileZipFolder = new System.Windows.Forms.ToolStripButton();
            this.tsbtnCopyCmd = new System.Windows.Forms.ToolStripButton();
            this.tsbtnShowFileTool = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsTipsText = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 35);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lvMain);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lvFileItem);
            this.splitContainer1.Size = new System.Drawing.Size(914, 586);
            this.splitContainer1.SplitterDistance = 304;
            this.splitContainer1.TabIndex = 1;
            // 
            // lvMain
            // 
            this.lvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvMain.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader6});
            this.lvMain.FullRowSelect = true;
            this.lvMain.HideSelection = false;
            this.lvMain.Location = new System.Drawing.Point(0, 28);
            this.lvMain.Name = "lvMain";
            this.lvMain.Size = new System.Drawing.Size(304, 558);
            this.lvMain.TabIndex = 0;
            this.lvMain.UseCompatibleStateImageBehavior = false;
            this.lvMain.View = System.Windows.Forms.View.Details;
            this.lvMain.SelectedIndexChanged += new System.EventHandler(this.lvMain_SelectedIndexChanged);
            this.lvMain.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvMain_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "名称";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "地址";
            this.columnHeader2.Width = 178;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "状态";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lvFileItem
            // 
            this.lvFileItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvFileItem.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvFileItem.FullRowSelect = true;
            this.lvFileItem.HideSelection = false;
            this.lvFileItem.Location = new System.Drawing.Point(0, 28);
            this.lvFileItem.Name = "lvFileItem";
            this.lvFileItem.Size = new System.Drawing.Size(606, 558);
            this.lvFileItem.TabIndex = 1;
            this.lvFileItem.UseCompatibleStateImageBehavior = false;
            this.lvFileItem.View = System.Windows.Forms.View.Details;
            this.lvFileItem.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvFileItem_MouseDoubleClick);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "序号";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "地址";
            this.columnHeader4.Width = 402;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "状态";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 131;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnFrmAddMultiple,
            this.btnStartAll,
            this.btnStopAll,
            this.btnRemoveAll,
            this.btnOpenFolder,
            this.btnDownloadSelect,
            this.toolStripButton1,
            this.btnStartDownload,
            this.tsbtnSearch,
            this.tsbtnReloadTs,
            this.tsbtnFileZipFolder,
            this.tsbtnCopyCmd,
            this.tsbtnShowFileTool});
            this.toolStrip1.Location = new System.Drawing.Point(0, 35);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(938, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnFrmAddMultiple
            // 
            this.btnFrmAddMultiple.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnFrmAddMultiple.Image = ((System.Drawing.Image)(resources.GetObject("btnFrmAddMultiple.Image")));
            this.btnFrmAddMultiple.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFrmAddMultiple.Name = "btnFrmAddMultiple";
            this.btnFrmAddMultiple.Size = new System.Drawing.Size(60, 22);
            this.btnFrmAddMultiple.Text = "批量添加";
            this.btnFrmAddMultiple.Click += new System.EventHandler(this.btnFrmAddMultiple_Click);
            // 
            // btnStartAll
            // 
            this.btnStartAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnStartAll.Image = ((System.Drawing.Image)(resources.GetObject("btnStartAll.Image")));
            this.btnStartAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStartAll.Name = "btnStartAll";
            this.btnStartAll.Size = new System.Drawing.Size(60, 22);
            this.btnStartAll.Text = "全部开始";
            this.btnStartAll.Click += new System.EventHandler(this.btnStartAll_Click);
            // 
            // btnStopAll
            // 
            this.btnStopAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnStopAll.Image = ((System.Drawing.Image)(resources.GetObject("btnStopAll.Image")));
            this.btnStopAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStopAll.Name = "btnStopAll";
            this.btnStopAll.Size = new System.Drawing.Size(60, 22);
            this.btnStopAll.Text = "全部停止";
            this.btnStopAll.Click += new System.EventHandler(this.btnStopAll_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnRemoveAll.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveAll.Image")));
            this.btnRemoveAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(60, 22);
            this.btnRemoveAll.Text = "移除选中";
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnOpenFolder
            // 
            this.btnOpenFolder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnOpenFolder.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFolder.Image")));
            this.btnOpenFolder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpenFolder.Name = "btnOpenFolder";
            this.btnOpenFolder.Size = new System.Drawing.Size(60, 22);
            this.btnOpenFolder.Text = "打开目录";
            this.btnOpenFolder.Click += new System.EventHandler(this.btnOpenFolder_Click);
            // 
            // btnDownloadSelect
            // 
            this.btnDownloadSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDownloadSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnDownloadSelect.Image")));
            this.btnDownloadSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDownloadSelect.Name = "btnDownloadSelect";
            this.btnDownloadSelect.Size = new System.Drawing.Size(60, 22);
            this.btnDownloadSelect.Text = "下载选中";
            this.btnDownloadSelect.ToolTipText = "下载选中";
            this.btnDownloadSelect.Click += new System.EventHandler(this.btnDownloadSelect_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(72, 22);
            this.toolStripButton1.Text = "文件合并器";
            this.toolStripButton1.ToolTipText = "打开文件合并器";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // btnStartDownload
            // 
            this.btnStartDownload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnStartDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnStartDownload.Image")));
            this.btnStartDownload.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStartDownload.Name = "btnStartDownload";
            this.btnStartDownload.Size = new System.Drawing.Size(60, 22);
            this.btnStartDownload.Text = "开始下载";
            this.btnStartDownload.ToolTipText = "开始下载全部数据";
            this.btnStartDownload.Click += new System.EventHandler(this.btnStartDownload_Click);
            // 
            // tsbtnSearch
            // 
            this.tsbtnSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnSearch.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnSearch.Image")));
            this.tsbtnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnSearch.Name = "tsbtnSearch";
            this.tsbtnSearch.Size = new System.Drawing.Size(60, 22);
            this.tsbtnSearch.Text = "资源搜索";
            this.tsbtnSearch.Click += new System.EventHandler(this.tsbtnSearch_Click);
            // 
            // tsbtnReloadTs
            // 
            this.tsbtnReloadTs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnReloadTs.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnReloadTs.Image")));
            this.tsbtnReloadTs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnReloadTs.Name = "tsbtnReloadTs";
            this.tsbtnReloadTs.Size = new System.Drawing.Size(74, 22);
            this.tsbtnReloadTs.Text = "重载TS文件";
            this.tsbtnReloadTs.Click += new System.EventHandler(this.tsbtnReloadTs_Click);
            // 
            // tsbtnFileZipFolder
            // 
            this.tsbtnFileZipFolder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnFileZipFolder.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnFileZipFolder.Image")));
            this.tsbtnFileZipFolder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnFileZipFolder.Name = "tsbtnFileZipFolder";
            this.tsbtnFileZipFolder.Size = new System.Drawing.Size(60, 22);
            this.tsbtnFileZipFolder.Text = "文件归档";
            this.tsbtnFileZipFolder.ToolTipText = "把下载好的视频移动到一个文件夹中";
            this.tsbtnFileZipFolder.Click += new System.EventHandler(this.tsbtnFileZipFolder_Click);
            // 
            // tsbtnCopyCmd
            // 
            this.tsbtnCopyCmd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnCopyCmd.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnCopyCmd.Image")));
            this.tsbtnCopyCmd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCopyCmd.Name = "tsbtnCopyCmd";
            this.tsbtnCopyCmd.Size = new System.Drawing.Size(60, 22);
            this.tsbtnCopyCmd.Text = "复制转码";
            this.tsbtnCopyCmd.ToolTipText = "复制转码指令";
            this.tsbtnCopyCmd.Click += new System.EventHandler(this.tsbtnCopyCmd_Click);
            // 
            // tsbtnShowFileTool
            // 
            this.tsbtnShowFileTool.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnShowFileTool.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnShowFileTool.Image")));
            this.tsbtnShowFileTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnShowFileTool.Name = "tsbtnShowFileTool";
            this.tsbtnShowFileTool.Size = new System.Drawing.Size(72, 22);
            this.tsbtnShowFileTool.Text = "文件查看器";
            this.tsbtnShowFileTool.Click += new System.EventHandler(this.tsbtnShowFileTool_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsTipsText});
            this.statusStrip1.Location = new System.Drawing.Point(0, 631);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(938, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsTipsText
            // 
            this.tsTipsText.Name = "tsTipsText";
            this.tsTipsText.Size = new System.Drawing.Size(62, 17);
            this.tsTipsText.Text = "状态：......";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(938, 653);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmMain";
            this.Style = Sunny.UI.UIStyle.Custom;
            this.Text = "Ts文件下载服务";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsTipsText;
        private System.Windows.Forms.ListView lvMain;
        private System.Windows.Forms.ToolStripButton btnStartDownload;
        private System.Windows.Forms.ListView lvFileItem;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ToolStripButton btnFrmAddMultiple;
        private System.Windows.Forms.ToolStripButton btnStartAll;
        private System.Windows.Forms.ToolStripButton btnStopAll;
        private System.Windows.Forms.ToolStripButton btnRemoveAll;
        private System.Windows.Forms.ToolStripButton btnOpenFolder;
        private System.Windows.Forms.ToolStripButton btnDownloadSelect;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton tsbtnShowFileTool;
        private System.Windows.Forms.ToolStripButton tsbtnReloadTs;
        private System.Windows.Forms.ToolStripButton tsbtnSearch;
        private System.Windows.Forms.ToolStripButton tsbtnFileZipFolder;
        private System.Windows.Forms.ToolStripButton tsbtnCopyCmd;
    }
}

