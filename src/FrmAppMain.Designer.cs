﻿
namespace WinFormTsService {
    partial class FrmAppMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tabContainer1 = new AtomFormUI.UControls.TabContainer();
            this.SuspendLayout();
            // 
            // tabContainer1
            // 
            this.tabContainer1.BackColor = System.Drawing.Color.SkyBlue;
            this.tabContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabContainer1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabContainer1.ListNav = new string[] {
        "首页",
        "搜索",
        "下载",
        "首页",
        "aaa",
        "bbb",
        "ccc"};
            this.tabContainer1.Location = new System.Drawing.Point(0, 0);
            this.tabContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabContainer1.Name = "tabContainer1";
            this.tabContainer1.NavActiveColor = System.Drawing.Color.Red;
            this.tabContainer1.NavNominalColor = System.Drawing.Color.Orange;
            this.tabContainer1.Size = new System.Drawing.Size(932, 552);
            this.tabContainer1.TabIndex = 0;
            this.tabContainer1.Title = "主窗体";
            this.tabContainer1.TitleFontSize = 14;
            this.tabContainer1.TabClick += new System.Action<System.Windows.Forms.Button, int>(this.tabContainer1_TabClick);
            // 
            // FrmAppMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 552);
            this.Controls.Add(this.tabContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmAppMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmAppMain";
            this.ResumeLayout(false);

        }

        #endregion

        private AtomFormUI.UControls.TabContainer tabContainer1;
    }
}