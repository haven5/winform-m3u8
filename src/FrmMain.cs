﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTsService.Models;
using WinFormTsService.Service;
using static System.Windows.Forms.ListView;

namespace WinFormTsService {
    public partial class FrmMain : Sunny.UI.UIForm {
        /// <summary>
        /// 文件列表
        /// </summary>
        public List<M3u8FileDto> M3u8Files = null;
        M3u8Factory Factory = null;
        public FrmMain() {
            InitializeComponent();
            this.Load += FrmMain_Load;
           
        }

        private void FrmMain_Load(object sender, EventArgs e) {
            FrmTips frm = new FrmTips();
            if(frm.ShowDialog() == DialogResult.OK) {
                Initialize();
            } else {
                this.Close();
            }

        }


        #region 0.1 初始化加载数据 +void Initialize()
        /// <summary>
        /// 初始化加载数据
        /// </summary>
        private void Initialize() {
            Factory = M3u8Factory.Instance;
            Factory.TsChange = this.UpdateStatusTsFile;
            Factory.FileChange = this.UpdateStatusFile;
            M3u8Files = new List<M3u8FileDto>();
            List<M3u8FileDto> files = M3u8FileConvert.ReadConfig();
            M3u8Files.AddRange(files);
            AddFiles(files);
        }
        #endregion 

        #region 1.0 批量添加数据
        private void btnFrmAddMultiple_Click(object sender, EventArgs e) {

            FrmAddMultiple frm = new FrmAddMultiple();
            if (frm.ShowDialog() != DialogResult.OK) {
                return;
            }
            int rowCount = 0;
            List<M3u8FileDto> m3u8Files = frm.M3u8Files;
            //检查添加是否重复
            List<M3u8FileDto> list = M3u8FileConvert.RemoveExists(M3u8Files, m3u8Files, out rowCount);
            M3u8Files.InsertRange(0, list);
            AddFiles(list, true);
            M3u8FileConvert.SaveConfig(M3u8Files);
            tsTipsText.Text = String.Format("成功新增：[{0}] 项，发现重复：[{0}] 项", m3u8Files.Count - rowCount, rowCount);
        }
        #endregion

        #region 1.1 添加视频 
        private void AddNewVideo(XmlVideo video) { 
            if(video.PlayList.Count == 0) {
                tsTipsText.Text = "暂无下载地址";
                return;
            }
            StringBuilder builder = new StringBuilder();
            foreach (XmlPlay item in video.PlayList) {
                builder.AppendFormat(item.Name);
                builder.AppendLine();
                builder.AppendFormat(item.Url);
                builder.AppendLine();

            }
            List<M3u8FileDto> list = M3u8FileConvert.M3u8Convert(builder.ToString()); 
            AddFiles(list, true);
            M3u8FileConvert.SaveConfig(M3u8Files);
        } 
        #endregion

        #region 2.0 添加文件 +AddFiles(List<M3u8FileDto> m3u8Files = null)
        /// <summary>
        /// 添加文件
        /// </summary>
        /// <param name="m3u8Files"></param>
        private void AddFiles(List<M3u8FileDto> m3u8Files = null, Boolean append = false) {
            if (m3u8Files == null) {
                return;
            }
            int rowIndex = lvMain.Items.Count;
            if (rowIndex > 0) {
                M3u8FileDto model = lvMain.Items[0].Tag as M3u8FileDto;
                rowIndex = model.Id;
            }
            foreach (M3u8FileDto dto in m3u8Files) {
                int index = rowIndex + dto.Id;
                ListViewItem item = new ListViewItem(index.ToString());
                dto.Id = index;
                item.Tag = dto;
                item.SubItems.Add(dto.Url);
                String status = M3u8FileDto.GetStatus(dto.Status);
                item.SubItems.Add(status);

                item.ToolTipText = String.Format("{0}-{1}", dto.FileId, dto.FileName);
                if (append) {
                    lvMain.Items.Insert(0, item);
                } else {
                    lvMain.Items.Add(item);
                }

            }
        }
        #endregion

        #region 3.0 开始下载文件
        private void btnStartDownload_Click(object sender, EventArgs e) {
            ListViewItemCollection list = lvMain.Items;
            List<M3u8FileDto> rows = new List<M3u8FileDto>();
            foreach (ListViewItem item in list) {
                M3u8FileDto dto = item.Tag as M3u8FileDto;
                if (dto == null) {
                    continue;
                }
                rows.Add(dto);
            }
            Factory.Add(rows);
        }
        #endregion

        #region 4.0 切换下载文件显示列表子文件
        private void lvMain_SelectedIndexChanged(object sender, EventArgs e) {
            SelectedListViewItemCollection list = lvMain.SelectedItems;
            if (list.Count == 0) {
                return;
            }
            if (list.Count > 1) {
                return;
            }
            ListViewItem row = list[0];
            M3u8FileDto dto = row.Tag as M3u8FileDto;
            M3u8FileDto current = lvFileItem.Tag as M3u8FileDto;
            if (dto == null) {
                lvFileItem.Items.Clear();
                return;
            }
            if (dto == current) {
                return;
            }
            lvFileItem.Items.Clear();
            lvFileItem.Tag = dto;
            if (dto.TsFiles == null || dto.TsFiles.Count == 0) {
                //加载子文件列表
                List<TsFileInfo> rows = M3u8FileConvert.GetUrlTsFiles(dto);
                dto.TsFiles = rows;
                if (rows.Count > 0) {
                    M3u8FileConvert.SaveConfig(M3u8Files);
                }
            }

            for (int i = 0; i < dto.TsFiles.Count; i++) {
                TsFileInfo file = dto.TsFiles[i];
                String status = M3u8FileDto.GetStatus(file.Status);
                ListViewItem item = new ListViewItem(file.Index.ToString("D4"));
                item.SubItems.Add(file.Url);
                item.SubItems.Add(status);
                item.Tag = file;
                lvFileItem.Items.Add(item);
            }


        }
        #endregion

        #region 5.0 更新TS文件状态 +void UpdateStatusTsFile(TsFileInfo ts , Exception ex)
        /// <summary>
        /// 更新TS文件状态
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="status"></param>
        public void UpdateStatusTsFile(TsFileInfo ts, Exception ex) {
            ListViewItemCollection items = lvFileItem.Items;
            UpdateWinUI(() => {
                foreach (ListViewItem item in items) {
                    TsFileInfo file = item.Tag as TsFileInfo;
                    if (file == null) {
                        continue;
                    }
                    if (!file.FileId.Equals(ts.FileId)) {
                        continue;
                    }
                    if (ex == null) {
                        item.SubItems[2].Text = M3u8FileDto.GetStatus(ts.Status);
                        return;

                    }
                    item.SubItems[2].Text = ex.Message;
                    return;
                }
            });
        }
        #endregion

        #region 5.1 更新m3u8文件状态 +void UpdateStatusFile(M3u8FileDto m3u8File,  Exception ex) 
        /// <summary>
        /// 更新m3u8文件状态
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="status"></param>
        public void UpdateStatusFile(M3u8FileDto m3u8File, Exception ex) {
            ListViewItemCollection items = lvMain.Items;
            UpdateWinUI(() => {
                foreach (ListViewItem item in items) {
                    M3u8FileDto file = item.Tag as M3u8FileDto;
                    if (file == null) {
                        continue;
                    }
                    if (!file.FileId.Equals(m3u8File.FileId)) {
                        continue;
                    }
                    String statusText = M3u8FileDto.GetStatus(m3u8File.Status);
                    if (ex == null) {
                        item.SubItems[2].Text = statusText;
                        return;
                    } else {
                        item.SubItems[2].Text = ex.Message;
                    }
                    return;
                }
            });
        }
        #endregion

        #region 6.0 更新界面数据 +void UpdateWinUI(Action action) 
        /// <summary>
        /// 更新界面数据
        /// </summary>
        /// <param name="action"></param>
        private void UpdateWinUI(Action action) {
            if (this.Disposing) {
                return;
            }
            if (this.IsDisposed) {
                return;
            }
            this.Invoke(action);
        }
        #endregion

        #region 7.0 保存配置信息 + void SaveConfig()
        /// <summary>
        /// 保存配置信息
        /// </summary>
        private void SaveConfig() {
            M3u8FileConvert.SaveConfig(M3u8Files);
        }

        #endregion

        #region 8.0 移除选中
        private void btnRemoveAll_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection list = lvMain.SelectedItems;
            foreach (ListViewItem item in list) {
                M3u8FileDto dto = item.Tag as M3u8FileDto;
                if (dto == null) {
                    continue;
                }
                M3u8Files.Remove(dto);
                lvMain.Items.Remove(item);
            }
            M3u8FileConvert.SaveConfig(M3u8Files);
        }
        #endregion

        #region 9.0 打开目录
        private void btnOpenFolder_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection list = lvMain.SelectedItems;
            if (list.Count == 0) {
                return;
            }
            ListViewItem row = list[0];
            M3u8FileDto dto = row.Tag as M3u8FileDto;
            if (dto == null) {
                return;
            }

            String path = M3u8FileConvert.GetM3u8Folder(dto.FileId);
            Process.Start("Explorer.exe", path);

        }
        #endregion

        #region 10.0 运行测试
        public void RunTest() {
            int times = 1;
            Task.Factory.StartNew(() => {
                while (times < 4) {
                    if (M3u8Files.Count == 0) {
                        Thread.Sleep(1000);
                        continue;
                    }
                    List<TsFileInfo> files = M3u8Files[0].TsFiles;
                    if (files == null) {
                        Thread.Sleep(1000);
                        continue;
                    }
                    foreach (TsFileInfo item in files) {

                        Thread.Sleep(1000);
                        UpdateStatusTsFile(item, null);
                    }
                }

            });
        }

        #endregion

        #region 11.0 下载选中
        private void btnDownloadSelect_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection list = lvMain.SelectedItems;
            foreach (ListViewItem item in list) {
                M3u8FileDto dto = item.Tag as M3u8FileDto;
                if (dto == null) {
                    continue;
                }
                Factory.Add(new List<M3u8FileDto>() { dto });
            }
        }
        #endregion

        #region 99.0 窗口关闭释放资源
        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e) {
            if(M3u8Files == null) {
                return;
            }
            foreach (M3u8FileDto item in M3u8Files) {
                if (item.Status == FileStatus.Download) {
                    item.Status = FileStatus.Abort;
                }
            }
            M3u8FileConvert.SaveConfig(M3u8Files);
            Factory.Dispose();

        }
        #endregion

        #region 11.0 文件合并器
        private void toolStripButton1_Click(object sender, EventArgs e) {
            FrmFilemMerge frm = new FrmFilemMerge();
            frm.Show();
        }
        #endregion

        #region 12.0 全部停止
        private void btnStopAll_Click(object sender, EventArgs e) {
            Factory.Dispose();
        }
        #endregion

        #region 13.0 文件查看器
        private void tsbtnShowFileTool_Click(object sender, EventArgs e) {
            FrmFileContent frm = new FrmFileContent();
            frm.Show();

        }
        #endregion

        #region 14.0 重载TS文件
        private void tsbtnReloadTs_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection rows = lvMain.SelectedItems;
            if (rows.Count == 0) {
                return;
            }
            ListViewItem row = rows[0];
            M3u8FileDto dto = row.Tag as M3u8FileDto;
            if (dto == null) {
                return;
            }
            dto.Status = FileStatus.Wait;
            lvFileItem.Items.Clear();
            List<TsFileInfo> list = M3u8FileConvert.GetUrlTsFiles(dto);
            dto.TsFiles = list;
            for (int i = 0; i < dto.TsFiles.Count; i++) {
                TsFileInfo file = dto.TsFiles[i];
                String status = M3u8FileDto.GetStatus(file.Status);
                ListViewItem item = new ListViewItem(file.Index.ToString("D4"));
                item.SubItems.Add(file.Url);
                item.SubItems.Add(status);
                item.Tag = file;
                lvFileItem.Items.Add(item);
            }

        }
        #endregion

        #region 15.0 全部开始
        private void btnStartAll_Click(object sender, EventArgs e) {
            ListViewItemCollection list = lvMain.Items;
            List<M3u8FileDto> rows = new List<M3u8FileDto>();
            foreach (ListViewItem item in list) {
                M3u8FileDto dto = item.Tag as M3u8FileDto;
                if (dto == null) {
                    continue;
                }
                rows.Add(dto);
            }
            Factory.Add(rows);
        }
        #endregion

        #region 16.0 资源搜索
        private void tsbtnSearch_Click(object sender, EventArgs e) {
            FrmSearch frm = new FrmSearch();
            frm.Download = this.AddNewVideo;
            frm.Show();
        }
        #endregion

        #region 17.0 文件归档
        private void tsbtnFileZipFolder_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection list = lvMain.SelectedItems;
            String filefolder = string.Empty;
            foreach (ListViewItem item in list) {
                M3u8FileDto dto = item.Tag as M3u8FileDto;
                if (dto == null) {
                    continue;
                }
                if (String.IsNullOrEmpty(filefolder)) {
                    filefolder =   String.Format("{0}/{1}", M3u8FileConvert.FOLDER_DOWNLOAD, dto.FileName);
                } 
                String folder = String.Format("{0}/{1}",M3u8FileConvert. FOLDER_DOWNLOAD ,dto.FileId);
                if ( !Directory.Exists(filefolder)) {
                    Directory.CreateDirectory(filefolder);
                }
                String[] files = Directory.GetFiles(folder, "*.mp4");
                foreach (String file in files) {
                    File.Move(file, String.Format("{0}/{1}", filefolder,Path.GetFileName(file)));
                }
            }
            tsTipsText.Text = "文件归档完成！";
        }
        #endregion

        #region 18.0 复制转码
        private void tsbtnCopyCmd_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection list = lvMain.SelectedItems;
            if(list.Count == 0) {
                return;
            }

            M3u8FileDto dto = list[0].Tag as M3u8FileDto;

            String folder = M3u8FileConvert.GetM3u8Folder(dto.FileId);
            String fileName = String.Format("{0}\\{1}.tsv", folder, dto.FileId);
            String destFileName = String.Format("{0}\\{1}.mp4", folder, dto.FileName);
            StringBuilder builder = new StringBuilder();
            builder.Append("./ffmpeg.exe");
            builder.Append($" -i \"{fileName}\" -vcodec copy -acodec copy -y \"{destFileName}\" ");
            Clipboard.SetText(builder.ToString());
            Process.Start("powershell.exe" ); 

        }
        #endregion

        #region 19.0 复制地址
        private void lvFileItem_MouseDoubleClick(object sender, MouseEventArgs e) {

            ListView view = sender as ListView;
            int row = view.SelectedItems.Count;
            if(row  == 0) {
                return;
            }
            TsFileInfo fileInfo = view.SelectedItems[0].Tag as TsFileInfo;
            Clipboard.SetText(fileInfo.Url);
            tsTipsText.Text = "复值地址";
        }
        #endregion

        private void lvMain_MouseDoubleClick(object sender, MouseEventArgs e) {
            ListView view = sender as ListView;
            int row = view.SelectedItems.Count;
            if (row == 0) {
                return;
            }
            M3u8FileDto fileInfo = view.SelectedItems[0].Tag as M3u8FileDto;
            Clipboard.SetText(fileInfo.Url);
            tsTipsText.Text = "复值地址";
        }
    }
}
