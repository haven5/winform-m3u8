﻿
namespace WinFormTsService {
    partial class FrmTips {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTips));
            this.lblTip = new System.Windows.Forms.Label();
            this.btnOk = new AtomFormUI.UControls.CircleButton();
            this.btnExit = new AtomFormUI.UControls.CircleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTip
            // 
            this.lblTip.Font = new System.Drawing.Font("微软雅黑", 16F);
            this.lblTip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblTip.Location = new System.Drawing.Point(53, 87);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(500, 170);
            this.lblTip.TabIndex = 0;
            this.lblTip.Text = "所有资源来自网上, 该软件不参与任何制作, 上传, 储存等内容,禁止传播违法资源.该软件仅供学习参考, 请于安装后24小时内删除.";
            // 
            // btnOk
            // 
            this.btnOk.AutoSize = true;
            this.btnOk.BgColor = System.Drawing.Color.Green;
            this.btnOk.FlatAppearance.BorderSize = 0;
            this.btnOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btnOk.Location = new System.Drawing.Point(351, 260);
            this.btnOk.Name = "btnOk";
            this.btnOk.Radius = 5;
            this.btnOk.SelectedColor = System.Drawing.Color.White;
            this.btnOk.Size = new System.Drawing.Size(114, 46);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "同意并使用";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnExit
            // 
            this.btnExit.AutoSize = true;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btnExit.Location = new System.Drawing.Point(97, 260);
            this.btnExit.Name = "btnExit";
            this.btnExit.Radius = 5;
            this.btnExit.Size = new System.Drawing.Size(114, 46);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "不同意";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(364, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "广告投放：QQ 1481662712";
            // 
            // FrmTips
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 349);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblTip);
            this.EscClose = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTips";
            this.Text = "软件使用协议";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTip;
        private AtomFormUI.UControls.CircleButton btnOk;
        private AtomFormUI.UControls.CircleButton btnExit;
        private System.Windows.Forms.Label label1;
    }
}