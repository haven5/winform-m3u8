﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTsService.Models;
using WinFormTsService.Service;

namespace WinFormTsService
{
    public partial class FrmVideoSites : Sunny.UI.UIForm {
        private List<SiteInfo> VideoSites { get; set; }
        public SiteInfo Current = null; 
        public FrmVideoSites( List<SiteInfo>  sites,String id ) {
            InitializeComponent();
            VideoSites = sites;
            LoadSties(id); 
        }

        #region 1.0 加载站点资源数据 +void LoadSties() 
        /// <summary>
        /// 加载站点资源数据
        /// </summary>
        private void LoadSties(String id) {
            flpContainer.Controls.Clear(); 
            List<RadioButton> list = new List<RadioButton>();
            foreach (SiteInfo item in VideoSites) {
                RadioButton button = new RadioButton();
                button.Text = item.Name;
                button.Tag = item;
                button.Checked = item.Id == id;
                button.Click += Button_Click;
                list.Add(button);
            }
            flpContainer.Controls.AddRange(list.ToArray());
            lblMsg.Text = String.Format("共计【{0}】个站点", VideoSites.Count);
        }

        private void Button_Click(object sender, EventArgs e) {
            RadioButton button = sender as RadioButton;
            SiteInfo current = button.Tag as SiteInfo;
            Current = current;
        }
        #endregion
         

        #region 3.0 确定选中的节点
        private void btnOk_Click(object sender, EventArgs e) {
            if (Current == null) {
                return;
            }
            DialogResult = DialogResult.OK;
        }
        #endregion


    }
}
