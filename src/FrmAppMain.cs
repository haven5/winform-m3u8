﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTsService.AppForms;

namespace WinFormTsService {
    public partial class FrmAppMain : Form {
        public FrmAppMain() {
            InitializeComponent();
        }

        private void tabContainer1_TabClick(Button button, int index) {

            UserControl control =  AppFunc.GetAppFunc(index);
          
            tabContainer1.SetView(control);
            Console.WriteLine("{0}=>{1}", button.Text,index);

        }
    }
}
