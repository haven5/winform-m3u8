﻿
namespace WinFormTsService {
    partial class FrmFilemMerge {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFilemMerge));
            this.lvMain = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnFrmAddMultiple = new System.Windows.Forms.ToolStripButton();
            this.btnStartAll = new System.Windows.Forms.ToolStripButton();
            this.btnClearFile = new System.Windows.Forms.ToolStripButton();
            this.btnRemoveAll = new System.Windows.Forms.ToolStripButton();
            this.btnOpenFolder = new System.Windows.Forms.ToolStripButton();
            this.btnDownFile = new System.Windows.Forms.ToolStripButton();
            this.btnUpFile = new System.Windows.Forms.ToolStripButton();
            this.btnMoveStart = new System.Windows.Forms.ToolStripButton();
            this.btnMoveEnd = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.txtTipText = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvMain
            // 
            this.lvMain.AllowDrop = true;
            this.lvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvMain.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvMain.FullRowSelect = true;
            this.lvMain.HideSelection = false;
            this.lvMain.Location = new System.Drawing.Point(0, 64);
            this.lvMain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lvMain.Name = "lvMain";
            this.lvMain.Size = new System.Drawing.Size(632, 553);
            this.lvMain.TabIndex = 1;
            this.lvMain.UseCompatibleStateImageBehavior = false;
            this.lvMain.View = System.Windows.Forms.View.Details;
            this.lvMain.DragDrop += new System.Windows.Forms.DragEventHandler(this.lvMain_DragDrop);
            this.lvMain.DragEnter += new System.Windows.Forms.DragEventHandler(this.lvMain_DragEnter);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "序号";
            this.columnHeader1.Width = 70;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "文件";
            this.columnHeader2.Width = 200;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnFrmAddMultiple,
            this.btnStartAll,
            this.btnClearFile,
            this.btnRemoveAll,
            this.btnOpenFolder,
            this.btnDownFile,
            this.btnUpFile,
            this.btnMoveStart,
            this.btnMoveEnd});
            this.toolStrip1.Location = new System.Drawing.Point(0, 35);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(641, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnFrmAddMultiple
            // 
            this.btnFrmAddMultiple.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnFrmAddMultiple.Image = ((System.Drawing.Image)(resources.GetObject("btnFrmAddMultiple.Image")));
            this.btnFrmAddMultiple.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFrmAddMultiple.Name = "btnFrmAddMultiple";
            this.btnFrmAddMultiple.Size = new System.Drawing.Size(60, 22);
            this.btnFrmAddMultiple.Text = "批量添加";
            this.btnFrmAddMultiple.Click += new System.EventHandler(this.btnFrmAddMultiple_Click);
            // 
            // btnStartAll
            // 
            this.btnStartAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnStartAll.Image = ((System.Drawing.Image)(resources.GetObject("btnStartAll.Image")));
            this.btnStartAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStartAll.Name = "btnStartAll";
            this.btnStartAll.Size = new System.Drawing.Size(36, 22);
            this.btnStartAll.Text = "合并";
            this.btnStartAll.Click += new System.EventHandler(this.btnStartAll_Click);
            // 
            // btnClearFile
            // 
            this.btnClearFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnClearFile.Image = ((System.Drawing.Image)(resources.GetObject("btnClearFile.Image")));
            this.btnClearFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClearFile.Name = "btnClearFile";
            this.btnClearFile.Size = new System.Drawing.Size(60, 22);
            this.btnClearFile.Text = "清空文件";
            this.btnClearFile.Click += new System.EventHandler(this.btnClearFile_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnRemoveAll.Image = ((System.Drawing.Image)(resources.GetObject("btnRemoveAll.Image")));
            this.btnRemoveAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(60, 22);
            this.btnRemoveAll.Text = "移除选中";
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnOpenFolder
            // 
            this.btnOpenFolder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnOpenFolder.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFolder.Image")));
            this.btnOpenFolder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOpenFolder.Name = "btnOpenFolder";
            this.btnOpenFolder.Size = new System.Drawing.Size(60, 22);
            this.btnOpenFolder.Text = "打开目录";
            this.btnOpenFolder.Click += new System.EventHandler(this.btnOpenFolder_Click);
            // 
            // btnDownFile
            // 
            this.btnDownFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDownFile.Image = ((System.Drawing.Image)(resources.GetObject("btnDownFile.Image")));
            this.btnDownFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDownFile.Name = "btnDownFile";
            this.btnDownFile.Size = new System.Drawing.Size(36, 22);
            this.btnDownFile.Text = "下移";
            this.btnDownFile.Click += new System.EventHandler(this.btnDownFile_Click);
            // 
            // btnUpFile
            // 
            this.btnUpFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnUpFile.Image = ((System.Drawing.Image)(resources.GetObject("btnUpFile.Image")));
            this.btnUpFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUpFile.Name = "btnUpFile";
            this.btnUpFile.Size = new System.Drawing.Size(36, 22);
            this.btnUpFile.Text = "上移";
            this.btnUpFile.Click += new System.EventHandler(this.btnUpFile_Click);
            // 
            // btnMoveStart
            // 
            this.btnMoveStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnMoveStart.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveStart.Image")));
            this.btnMoveStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMoveStart.Name = "btnMoveStart";
            this.btnMoveStart.Size = new System.Drawing.Size(60, 22);
            this.btnMoveStart.Text = "移到开头";
            this.btnMoveStart.Click += new System.EventHandler(this.btnMoveStart_Click);
            // 
            // btnMoveEnd
            // 
            this.btnMoveEnd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnMoveEnd.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveEnd.Image")));
            this.btnMoveEnd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMoveEnd.Name = "btnMoveEnd";
            this.btnMoveEnd.Size = new System.Drawing.Size(60, 22);
            this.btnMoveEnd.Text = "移到末尾";
            this.btnMoveEnd.Click += new System.EventHandler(this.btnMoveEnd_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtTipText});
            this.statusStrip1.Location = new System.Drawing.Point(0, 615);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(641, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // txtTipText
            // 
            this.txtTipText.Name = "txtTipText";
            this.txtTipText.Size = new System.Drawing.Size(56, 17);
            this.txtTipText.Text = "状态：....";
            // 
            // FrmFilemMerge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 637);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lvMain);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmFilemMerge";
            this.Text = "文件合并器";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvMain;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnFrmAddMultiple;
        private System.Windows.Forms.ToolStripButton btnStartAll;
        private System.Windows.Forms.ToolStripButton btnRemoveAll;
        private System.Windows.Forms.ToolStripButton btnOpenFolder;
        private System.Windows.Forms.ToolStripButton btnUpFile;
        private System.Windows.Forms.ToolStripButton btnDownFile;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel txtTipText;
        private System.Windows.Forms.ToolStripButton btnClearFile;
        private System.Windows.Forms.ToolStripButton btnMoveStart;
        private System.Windows.Forms.ToolStripButton btnMoveEnd;
    }
}