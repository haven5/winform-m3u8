﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormTsService {
    public partial class FrmFileContent : Sunny.UI.UIForm {
        public FrmFileContent() {
            InitializeComponent();
        }

        #region 显示文件
        private void tsbtnShowFile_Click(object sender, EventArgs e) {
            String url = txtPath.Text;
            if (String.IsNullOrEmpty(url)) {
                return;
            }
            WebClient client = new WebClient();
            try {
               byte[] buffer  = client.DownloadData(url);
                String contents = Encoding.UTF8.GetString(buffer);
                txtMessage.Text = contents;

            } finally {
                client.Dispose();
            }
        }

        #endregion

        #region 清空
        private void tsbtnClearAll_Click(object sender, EventArgs e) {
            txtMessage.Clear();
        } 
        #endregion
    }
}
