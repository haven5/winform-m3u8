﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTsService.Models;
using WinFormTsService.Service;
using static System.Windows.Forms.ListView;

namespace WinFormTsService {
    public partial class FrmFilemMerge : Sunny.UI.UIForm {
        public FrmFilemMerge() {
            InitializeComponent();
        }

        #region 1.0 拖拽开始
        private void lvMain_DragEnter(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.Link;
        }
        #endregion

        #region 2.0 拖拽完成
        private void lvMain_DragDrop(object sender, DragEventArgs e) {
            String[] files = (String[])e.Data.GetData(DataFormats.FileDrop);
            AddFiles(files);

        }
        #endregion

        #region 2.1 添加文件
        private void AddFiles(String[] files) {
            int index = 1;
            foreach (String file in files) {
                ListViewItem item = new ListViewItem();
                item.Tag = file;
                item.Text = index++.ToString("D4");
                item.SubItems.Add(Path.GetFileName(file));
                lvMain.Items.Add(item);
            }
        } 
        #endregion

        #region 3.0 清空文件
        private void btnClearFile_Click(object sender, EventArgs e) {
            lvMain.Items.Clear();
        }
        #endregion

        #region 4.0 选中项上移
        private void btnUpFile_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection rows = lvMain.SelectedItems;
            if (rows.Count == 0) {
                return;
            }
            List<ListViewItem> list = new List<ListViewItem>();
            int rowIndex  = rows[0].Index; 
            if(rowIndex == 0) {
                return;
            }
            foreach (ListViewItem item in rows) {
                list.Add(item);
                lvMain.Items.Remove(item);
            }
            rowIndex--;
            foreach (ListViewItem item in list) {
                lvMain.Items.Insert(rowIndex, item);
            }
        }
        #endregion

        #region 5.0 批量添加文件
        private void btnFrmAddMultiple_Click(object sender, EventArgs e) {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.SelectedPath = Directory.GetCurrentDirectory();
            if (dialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            String folder = dialog.SelectedPath;
            String[] files = Directory.GetFiles(folder);
            AddFiles(files);
        }
        #endregion

        #region 6.0 选中项下移
        private void btnDownFile_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection rows = lvMain.SelectedItems;
            if (rows.Count == 0) {
                return;
            }
            int rowIndex =  rows[0].Index;  
            List<ListViewItem> list = new List<ListViewItem>(); 
            foreach (ListViewItem item in rows) {
                list.Add(item);
                lvMain.Items.Remove(item);
            }
            rowIndex++;
            foreach (ListViewItem item in list) {
                lvMain.Items.Insert(rowIndex, item);
            }
        }
        #endregion

        #region 7.0 移动到开头
        private void btnMoveStart_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection rows = lvMain.SelectedItems;
            if (rows.Count == 0) {
                return;
            }
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (ListViewItem item in rows) {
                list.Add(item);
                lvMain.Items.Remove(item);
            }
            list.Reverse();
            foreach (ListViewItem item in list) {
                lvMain.Items.Insert(0,item);
            }
        } 
        #endregion

        #region 8.0 移动到末尾
        private void btnMoveEnd_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection rows = lvMain.SelectedItems;
            if (rows.Count == 0) {
                return;
            }
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (ListViewItem item in rows) {
                list.Add(item);
                lvMain.Items.Remove(item);
            }
            foreach (ListViewItem item in list) {
                lvMain.Items.Add(item);
            }
        }
        #endregion

        #region 9.0 开始合并文件
        private void btnStartAll_Click(object sender, EventArgs e) {
            List<String> files = new List<string>(); 
            foreach (ListViewItem item in lvMain.Items) {
                String file = item.Tag as String;
                files.Add(file);
            } 
            String fileName = M3u8FileConvert.MergeFiles(files); 
            txtTipText.Text = String.Format("文件合并成功:{0}", fileName);
        }
        #endregion

        #region 10.0 移除选中项目
        private void btnRemoveAll_Click(object sender, EventArgs e) {
            SelectedListViewItemCollection rows = lvMain.SelectedItems;
            if (rows.Count == 0) {
                return;
            } 
            foreach (ListViewItem item in rows) { 
                lvMain.Items.Remove(item);
            }
        }
        #endregion

        #region 11.0 打开目录
        private void btnOpenFolder_Click(object sender, EventArgs e) {
            String folder = M3u8FileConvert.GetM3u8Folder(M3u8FileConvert.FOLDER_MERGE);
            Process.Start("explorer.exe", folder);
        } 
        #endregion
    }
}
