﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormTsService.Models
{
   public  class SiteInfo
    {
        public String Id { get; set; }
        public String Key { get; set; }
        public String Name { get; set; }
        public String Api { get; set; } 
    }
}
