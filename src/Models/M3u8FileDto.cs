﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormTsService.Models {


    /// <summary>
    /// 文件状态
    /// </summary>
    public enum FileStatus {
        /// <summary>
        /// 等待中
        /// </summary>
        Wait = 1,
        /// <summary>
        /// 下载中
        /// </summary>
        Download = 2,
        /// <summary>
        /// 已完成
        /// </summary>
        Complete = 3,
        /// <summary>
        /// 转码中
        /// </summary>
        Convert = 4,
        /// <summary>
        /// 下载错误
        /// </summary>
        Error = 5,
        /// <summary>
        /// 终止下载
        /// </summary>
        Abort = 6,
    }

    public class M3u8FileDto { 


        public M3u8FileDto() {
            CryptKey = new Dictionary<string, String>();
        }
        /// <summary>
        /// 序号
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 连接地址
        /// </summary>
        public String Url { get; set; }
        /// <summary>
        /// 文件名
        /// </summary>
        public String FileName { get; set; }
        /// <summary>
        /// 文件主键
        /// </summary>
        public String FileId { get; set; }
        /// <summary>
        /// 文件状态
        /// </summary>
        public FileStatus Status { get; set; } 
        /// <summary>
        /// 是否加密
        /// </summary>
        public Boolean IsCrypt { get; set; }
        /// <summary>
        /// 加密算法
        /// </summary>
        public String Method { get; set; }
        /// <summary>
        /// 解密密钥 Aes-128 解密的key 和iv 一致
        /// </summary>
        public Dictionary<String, String>   CryptKey { get; set; }
        /// <summary>
        /// 子文件列表
        /// </summary>
        public List<TsFileInfo> TsFiles { get; set; } 
        /// <summary>
        /// 获取文件状态
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static String GetStatus(FileStatus status) {
            switch (status) {
                case FileStatus.Wait:
                    return "等待"; 
                case FileStatus.Download:
                    return "下载中";
                case FileStatus.Complete:
                    return "完成";
                case FileStatus.Convert:
                    return "转码中";
                case FileStatus.Error:
                    return "下载错误";
                case FileStatus.Abort:
                    return "终止下载";
                default:
                    return "--";
            }
        }


    }
}
