﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Drawing;

namespace WinFormTsService.Models
{
    /// <summary>
    /// 数据页
    /// </summary>
    public class PageList
    {
        public PageList() {
            Data = new List<XmlVideo>();
            Types = new List<VideoClass>();
        }
        public int Page { get; set; }
        public long TotalCount { get; set; }
        public List<XmlVideo> Data { get; set; }
        /// <summary>
        /// 视频分类信息
        /// </summary>
        public List<VideoClass> Types{ get; set; }
        public int PageSize { get; set; } 
    }
    /// <summary>
    /// 视频分类信息
    /// </summary>
    public class VideoClass
    {
        public String Id { get; set; }
        public String Name { get; set; }
    }

    /// <summary>
    /// 视频文件信息
    /// </summary> 
    public class XmlVideo
    {
        public XmlVideo() {
            PlayList = new List<XmlPlay>();
        }
        [Description("last")]
        public String Last { get; set; }
        [Description("id")]
        public String Id { get; set; }
        [Description("tid")]
        public String TypeId { get; set; } 
        [Description("name")]
        public String Name { get; set; }
        /// <summary>
        /// 分类
        /// </summary>
        [Description("type")]
        public String Type { get; set; }
        [Description("pic")]
        public String FaceUrl { get; set; }
        public Image Face { get; set; }
        [Description("lang")]
        public String Lang { get; set; }
        [Description("area")]
        public String Area { get; set; }
        [Description("year")]
        public String Year { get; set; }
        [Description("node")]
        public String Note { get; set; }
        [Description("actor")]
        public String Actor { get; set; }
        /// <summary>
        /// director
        /// </summary>
        [Description("director")]
        public String Director { get; set; }
        /// <summary>
        /// 说明 
        /// </summary>
        [Description("des")]
        public String Desc { get; set; }
        /// <summary>
        /// 播放列表 dl dd
        /// </summary>
        public List<XmlPlay> PlayList { get; set; }

    }

    public class XmlPlay
    {
        public int Index { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// 播放地址
        /// </summary>
        public String Url { get; set; }
    }
}
