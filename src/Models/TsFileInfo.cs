﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormTsService.Models {
    public class TsFileInfo {

        /// <summary>
        /// 文件序号
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
        public String FileName { get; set; }
        /// <summary>
        /// 文件唯一标识
        /// </summary>
        public String FileId { get; set; }
        /// <summary>
        /// 文件网络地址
        /// </summary>
        public String Url { get; set; }
        /// <summary>
        /// 文件状态
        /// </summary>
        public FileStatus Status { get; set; }
        
    }
}
