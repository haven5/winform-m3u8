﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinFormTsService.Models;
using WinFormTsService.Service;

namespace WinFormTsService
{
    public partial class FrmSearch : Sunny.UI.UIForm {
        List<SiteInfo> VideoSites = null;
       VideoSiteService SiteService = new VideoSiteService();
        Bitmap bitmap = null;   

        public Action<XmlVideo> Download { get; set; }

        public FrmSearch() {
            InitializeComponent();
            SiteService.ImageChange = (data) => {
                UpdateWinUI(() => { 

                    SetViewImage (data);

                });
            };
            SiteService.PageChange = (page)=> {
                UpdateWinUI(() => {
                    this.FillPageData(page);
                    tsTipText.Text = String.Format("共计：{0}/{1} 条数据", dgvMain.Rows.Count, page.TotalCount);
                });
            };
            SiteService.TypeChange = (data) => {
                UpdateWinUI(() => { UpdateTypes(data); });
            };
            SiteService.ShowMessage = (msg) => {
                if (msg.Length > 20) {
                    MessageBox.Show(msg);
                    return;
                }

                UpdateWinUI(() => tsTipText.Text = msg);

            };
            this.FormClosing += FrmSearch_FormClosing;
            LoadSties();


              bitmap = new Bitmap(100, 100);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.FillRectangle(Brushes.Gray, new RectangleF(0, 0, bitmap.Width, bitmap.Height));
            graphics.DrawString("图片加载失败！", SystemFonts.DefaultFont, Brushes.White, new PointF(10, bitmap.Height / 2));
            graphics.Dispose(); 
        }

        #region 窗口关闭
        private void FrmSearch_FormClosing(object sender, FormClosingEventArgs e) {
            SiteService.ImageChange = null;
        }
        #endregion

        #region 1.0 加载站点资源数据 +void LoadSties() 
        /// <summary>
        /// 加载站点资源数据
        /// </summary>
        private void LoadSties() {
            List<SiteInfo> sites = SiteService.LoadLocalSiteInfo();
            VideoSites = sites;
            if (sites.Count > 0) {
                SiteInfo site = sites[0];
                SiteService.SiteId = site.Id;
                SiteService.Host = site.Api;
                tslblMsg.Text = String.Format("当前站点【{0}】", site.Name);
                Task.Factory.StartNew(() => {
                    SiteService.GetClass();
                });
            }
            //XmlVideo video = new XmlVideo();
            //video.Name = "测试数据";
            //video.FaceUrl = "AAAA";
            //Image bitmap = Image.FromFile(@"D:\Cache\0922.jpg");
            //bitmap = bitmap.GetThumbnailImage(200, 300, new Image.GetThumbnailImageAbort(() => { return false; }), IntPtr.Zero);
            //video.Face = bitmap;
            //SearchChange(video);
        }
        #endregion

        #region 2.0 搜索资源
        private void btnSearch_Click(object sender, EventArgs e) {
            Button button = sender as Button;
            button.Enabled = false;
            String keyword = txtkeyword.Text;
            SiteService.PageIndex = 1;
            dgvMain.Rows.Clear();
            Task.Factory.StartNew(() => {
                SiteService.GetList(keyword);
                UpdateWinUI(() => {
                    button.Enabled = true;
                });
            });

            //SiteService.GetClass();
        }
        #endregion

        #region 4.0 搜索的资源返回数据 + void FillPageData(PageList page) 
        /// <summary>
        /// 搜索的资源返回数据
        /// </summary>
        /// <param name="page"></param>
        public void FillPageData(PageList page) {

            foreach (XmlVideo video in page.Data) { 
                DataGridViewRow row = new DataGridViewRow();
                row.Cells.Add(new DataGridViewTextBoxCell() { });
                row.Cells.Add(new DataGridViewTextBoxCell() { });
                row.Cells.Add(new DataGridViewTextBoxCell() { });
                row.Cells.Add(new DataGridViewTextBoxCell() { });
                row.Cells.Add(new DataGridViewImageCell() { ImageLayout = DataGridViewImageCellLayout.Zoom });
                row.Cells[0].Value = video.Id;
                row.Cells[1].Value = dgvMain.Rows.Count + 1;
                row.Cells[2].Value = video.Name;

                StringBuilder builder = new StringBuilder();
                foreach (XmlPlay item in video.PlayList) {
                    builder.AppendFormat(item.Name);
                    builder.AppendLine();
                    builder.AppendFormat(item.Url);
                    builder.AppendLine(); 
                }
                if (video.PlayList.Count == 1) {
                    builder.Clear();
                    builder.Append(video.PlayList[0].Url);
                }
                row.Cells[3].Value = builder.ToString();
                row.Cells[3].ToolTipText = builder.ToString();
                row.Tag = video;
                row.Height = 60; 
                dgvMain.Rows.Insert(0, row);

            }

          
        }
        #endregion

        #region 5.0 搜索的资源返回数据 + void SetViewImage(XmlVideo video)
        /// <summary>
        /// 搜索的资源返回数据
        /// </summary>
        /// <param name="video"></param>
        public void SetViewImage(XmlVideo video) { 
            foreach (DataGridViewRow row in dgvMain.Rows) {
                if (row.Cells[0].Value.ToString().Equals(video.Id)) {

                    DataGridViewImageCell cell = row.Cells[4] as DataGridViewImageCell;
                    cell.Value = video.Face;
                    if (video.Face == null) { 
                        cell.Value = bitmap;
                    }
                    return;
                }

            }  
        }
        #endregion

        #region 6.0 更新分类信息
        private void UpdateTypes(List<VideoClass> list) {
            int index = 0;
            flowLayoutpaneltypes.Controls.Clear();
            RadioButton button = new RadioButton();
            button.Location = new Point(button.Width * index, 15);
            button.Click += TypeButton_Click;
            button.Text = "全部";
            button.Tag = new VideoClass() { Id = string.Empty, Name = "全部" };
            flowLayoutpaneltypes.Controls.Add(button);

            foreach (VideoClass item in list) {
                button = new RadioButton();
                button.Location = new Point(button.Width * index, 15);
                button.Click += TypeButton_Click;
                button.Text = item.Name;
                button.Tag = item;
                flowLayoutpaneltypes.Controls.Add(button);
                index++;
            }
        }

        private void TypeButton_Click(object sender, EventArgs e) {
            RadioButton button = sender as RadioButton;

            VideoClass type = button.Tag as VideoClass;
            SiteService.VideoType = type.Id;

        }
        #endregion

        #region 7.0 更新界面 +void UpdateWinUI(Action action)
        private void UpdateWinUI(Action action) {
            if (this.Disposing) {
                return;
            }
            if (this.IsDisposed) {
                return;
            }
            this.Invoke(action);
        }


        #endregion

        #region 8.0 导入站点资源
        private void tsbtnAdd_Click(object sender, EventArgs e) {
            ToolStripButton button = sender as ToolStripButton;
            String url = txtkeyword.Text;
            if (String.IsNullOrEmpty(url)) {
                MessageBox.Show("请在搜索栏中输入地址！");
                return;
            }
            button.Enabled = false;
            Task.Factory.StartNew(() => {
                List<SiteInfo> sites = SiteService.LoadSiteInfo(url);
                UpdateWinUI(() => {
                    tslblMsg.Text = $"导入站点【{sites.Count }】个";
                });
            });



        }

        #endregion

        #region 9.0 复制选中
        private void tsbtnCopyUrl_Click(object sender, EventArgs e) {

            DataGridViewSelectedRowCollection rows = dgvMain.SelectedRows;
            if (rows.Count == 0) {
                return;
            }
            DataGridViewRow row = rows[0];

            XmlVideo video = row.Tag as XmlVideo;

            if (video == null) {
                return;
            }
            StringBuilder builder = new StringBuilder();
            foreach (XmlPlay item in video.PlayList) {
                builder.AppendFormat(item.Name);
                builder.AppendLine();
                builder.AppendFormat(item.Url);
                builder.AppendLine();

            }
            Clipboard.SetText(builder.ToString());
            tsTipText.Text = "复制成功！";
        }
        #endregion

        #region 10.0 下一页
        private void tsbtnNextPage_Click(object sender, EventArgs e) {

            ToolStripButton button = sender as ToolStripButton;
            button.Enabled = false;
            SiteService.PageIndex++;
            String keyword = txtkeyword.Text;
            Task.Factory.StartNew(() => {

                SiteService.GetList(keyword);
                UpdateWinUI(() => {
                    button.Enabled = true;
                });
            });
        }
        #endregion

        #region 11.0 显示站点列表
        private void tsbtnSites_Click(object sender, EventArgs e) {
            FrmVideoSites frm = new FrmVideoSites(VideoSites, SiteService.SiteId);
            if (frm.ShowDialog() == DialogResult.OK) {
                SiteInfo site = frm.Current;
                SiteService.Host = site.Api;
                SiteService.SiteId = site.Id;
                Task.Factory.StartNew(() => {
                    SiteService.GetClass();
                });
                tslblMsg.Text = $"当前站点：【{site.Name}】";
            }

        }
        #endregion

        #region 12.0 显示预览图
        private void dgvMain_CellMouseEnter(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex == -1) {
                return;
            }
            if (e.ColumnIndex < 4) {
                return;
            }
            dgvMain.Tag = true;
            DataGridViewRow row = dgvMain.Rows[e.RowIndex];
            XmlVideo video = row.Tag as XmlVideo;
            FrmImageView.Instance.ShowImage(video.Face,video.FaceUrl);
            //Console.WriteLine(video.Name);

        }

        private void dgvMain_CellMouseLeave(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex == -1) {
                return;
            }
            dgvMain.Tag = false;
            Task.Factory.StartNew(() => {
                Thread.Sleep(1000);
                UpdateWinUI(()=> {
                    if ((Boolean)dgvMain.Tag) {
                        return;
                    }
                    FrmImageView.Instance.Close();
                });
            });
            
        }

        #endregion

        #region 13.0 导出PotPlayer列表
        private void tsbtnPotPlayer_Click(object sender, EventArgs e) {
            /*
             
            DAUMPLAYLIST
            topindex=0
            saveplaypos=0
            1*file*https://cdn.wlcdn99.com:777/7eda2561/index.m3u8
            1*title*第一集
             */


            DataGridViewSelectedRowCollection rows = dgvMain.SelectedRows;
            if (rows.Count == 0) {
                return;
            }
            DataGridViewRow row = rows[0];

            XmlVideo video = row.Tag as XmlVideo;

            if (video == null) {
                return;
            }
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("DAUMPLAYLIST");
            builder.AppendLine("topindex=0");
            builder.AppendLine("saveplaypos=0");
            int index = 1;
            foreach (XmlPlay item in video.PlayList) {
                builder.AppendFormat("{0}*file*{1}", index, item.Url);
                builder.AppendLine();
                builder.AppendFormat("{0}*title*{1}", index,item.Name);
                builder.AppendLine();
                index++;
            }
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = String.Format("{0}.dpl",video.Name);
            dialog.Filter = "所有文件|*.*|播放列表|*.dpl";//  String.Format("{0}.dpl",video.Name);
            if (dialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            File.WriteAllText(dialog.FileName, builder.ToString(), Encoding.UTF8);
            Clipboard.SetText(builder.ToString());
            tsTipText.Text = "保存成功！";
        }
        #endregion

        #region 14.0 下载选中
        private void btnDownload_Click(object sender, EventArgs e) {

            DataGridViewSelectedRowCollection rows = dgvMain.SelectedRows;
            if (rows.Count == 0) {
                return;
            }
            DataGridViewRow row = rows[0];

            XmlVideo video = row.Tag as XmlVideo;

            if (video == null) {
                return;
            }

            Download?.Invoke(video); 
        } 
        #endregion
    }
}
