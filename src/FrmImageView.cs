﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormTsService
{
    public partial class FrmImageView : Form
    {
        private FrmImageView() {
            InitializeComponent();
        }
        private String Id { get; set; }
        static FrmImageView instance = null;

        public static FrmImageView Instance {
            get {
                if (instance == null || instance.IsDisposed) {
                    instance = new FrmImageView();
                }
                return instance;
            }
        }

        #region 1.0 显示图像
        /// <summary>
        /// 显示图像
        /// </summary>
        /// <param name="image"></param>
        public void ShowImage(Image image, String key) {
            if (Id == key) {
                return;
            }
            if (image == null) {
                return;
            }
            Id = key;
            //Console.WriteLine("key=>{0}",key);
            picMain.BackgroundImage = image;
            //Console.WriteLine(image.Height);
            this.Show();
            Point point = MousePosition;
            int width = Screen.PrimaryScreen.WorkingArea.Width - 10;
            int height = Screen.PrimaryScreen.WorkingArea.Height - 10;
            //Console.WriteLine("m=>{0},{1}", point.X, point.Y);
            if (width - point.X < image.Width) {
                point.X = point.X - Width;
                //Console.WriteLine("width=>{0}", point.X);
            }
            if (height - point.Y < image.Height) {
                point.Y = point.Y -  Height;
                //point.Y = height - (Location.Y + point.Y);

                //Console.WriteLine("Height=>{0}", point.Y);
            }
            Location = new Point(point.X, point.Y);
            lblFileName.Width = image.Width - 10;
            this.lblFileName.Text = key;
            //this.Size = new Size(image.Width, image.Height);
            //Console.WriteLine("{0},{1}", point.X, point.Y);
        }
        #endregion

    }
}
