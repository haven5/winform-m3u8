﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormTsService.AppForms {
    public class AppFunc {

        static List<Type> Apps = new List<Type>() { 
        typeof(Download),
        typeof(Search),
        };

        public static UserControl GetAppFunc(int index) {
            if(index>= Apps.Count) {
                return null; 
            }

            Type t = Apps[index];
            UserControl control =(UserControl) Activator.CreateInstance(t);
            return control;
        }

    }
}
