﻿
namespace WinFormTsService {
    partial class FrmSearch {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSearch));
            this.label1 = new System.Windows.Forms.Label();
            this.txtkeyword = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsTipText = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnDownload = new System.Windows.Forms.ToolStripButton();
            this.tsbtnCopyUrl = new System.Windows.Forms.ToolStripButton();
            this.tsbtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tslblMsg = new System.Windows.Forms.ToolStripLabel();
            this.tsbtnSites = new System.Windows.Forms.ToolStripButton();
            this.tsbtnPotPlayer = new System.Windows.Forms.ToolStripButton();
            this.tsbtnNextPage = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvMain = new System.Windows.Forms.DataGridView();
            this.ColumnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnOrderNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFace = new System.Windows.Forms.DataGridViewImageColumn();
            this.gpTypes = new System.Windows.Forms.GroupBox();
            this.flowLayoutpaneltypes = new System.Windows.Forms.FlowLayoutPanel();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).BeginInit();
            this.gpTypes.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "关键字：";
            // 
            // txtkeyword
            // 
            this.txtkeyword.Location = new System.Drawing.Point(62, 19);
            this.txtkeyword.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtkeyword.Name = "txtkeyword";
            this.txtkeyword.Size = new System.Drawing.Size(497, 25);
            this.txtkeyword.TabIndex = 2;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(563, 18);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(52, 28);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "搜索";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsTipText});
            this.statusStrip1.Location = new System.Drawing.Point(0, 628);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 11, 0);
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsTipText
            // 
            this.tsTipText.Name = "tsTipText";
            this.tsTipText.Size = new System.Drawing.Size(56, 17);
            this.tsTipText.Text = "状态：....";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnDownload,
            this.tsbtnCopyUrl,
            this.tsbtnAdd,
            this.tslblMsg,
            this.tsbtnSites,
            this.tsbtnPotPlayer,
            this.tsbtnNextPage});
            this.toolStrip1.Location = new System.Drawing.Point(0, 32);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnDownload
            // 
            this.btnDownload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(60, 22);
            this.btnDownload.Text = "下载选中";
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // tsbtnCopyUrl
            // 
            this.tsbtnCopyUrl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnCopyUrl.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnCopyUrl.Image")));
            this.tsbtnCopyUrl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCopyUrl.Name = "tsbtnCopyUrl";
            this.tsbtnCopyUrl.Size = new System.Drawing.Size(60, 22);
            this.tsbtnCopyUrl.Text = "复制选中";
            this.tsbtnCopyUrl.Click += new System.EventHandler(this.tsbtnCopyUrl_Click);
            // 
            // tsbtnAdd
            // 
            this.tsbtnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnAdd.Image")));
            this.tsbtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnAdd.Name = "tsbtnAdd";
            this.tsbtnAdd.Size = new System.Drawing.Size(60, 22);
            this.tsbtnAdd.Text = "导入资源";
            this.tsbtnAdd.Click += new System.EventHandler(this.tsbtnAdd_Click);
            // 
            // tslblMsg
            // 
            this.tslblMsg.Name = "tslblMsg";
            this.tslblMsg.Size = new System.Drawing.Size(99, 22);
            this.tslblMsg.Text = "共计【0】个站点";
            // 
            // tsbtnSites
            // 
            this.tsbtnSites.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnSites.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnSites.Image")));
            this.tsbtnSites.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnSites.Name = "tsbtnSites";
            this.tsbtnSites.Size = new System.Drawing.Size(60, 22);
            this.tsbtnSites.Text = "站点列表";
            this.tsbtnSites.Click += new System.EventHandler(this.tsbtnSites_Click);
            // 
            // tsbtnPotPlayer
            // 
            this.tsbtnPotPlayer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnPotPlayer.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnPotPlayer.Image")));
            this.tsbtnPotPlayer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnPotPlayer.Name = "tsbtnPotPlayer";
            this.tsbtnPotPlayer.Size = new System.Drawing.Size(90, 22);
            this.tsbtnPotPlayer.Text = "PotPlayer列表";
            this.tsbtnPotPlayer.ToolTipText = "导出PotPlayer列表";
            this.tsbtnPotPlayer.Click += new System.EventHandler(this.tsbtnPotPlayer_Click);
            // 
            // tsbtnNextPage
            // 
            this.tsbtnNextPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbtnNextPage.Image = ((System.Drawing.Image)(resources.GetObject("tsbtnNextPage.Image")));
            this.tsbtnNextPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnNextPage.Name = "tsbtnNextPage";
            this.tsbtnNextPage.Size = new System.Drawing.Size(48, 22);
            this.tsbtnNextPage.Text = "下一页";
            this.tsbtnNextPage.Click += new System.EventHandler(this.tsbtnNextPage_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtkeyword);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Location = new System.Drawing.Point(5, 62);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Size = new System.Drawing.Size(790, 55);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // dgvMain
            // 
            this.dgvMain.AllowUserToAddRows = false;
            this.dgvMain.AllowUserToDeleteRows = false;
            this.dgvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMain.BackgroundColor = System.Drawing.Color.White;
            this.dgvMain.ColumnHeadersHeight = 30;
            this.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvMain.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnId,
            this.ColumnOrderNum,
            this.ColumnName,
            this.ColumnUrl,
            this.ColumnFace});
            this.dgvMain.GridColor = System.Drawing.Color.WhiteSmoke;
            this.dgvMain.Location = new System.Drawing.Point(2, 239);
            this.dgvMain.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dgvMain.Name = "dgvMain";
            this.dgvMain.ReadOnly = true;
            this.dgvMain.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightBlue;
            this.dgvMain.RowTemplate.Height = 60;
            this.dgvMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMain.Size = new System.Drawing.Size(792, 389);
            this.dgvMain.TabIndex = 7;
            this.dgvMain.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMain_CellMouseEnter);
            this.dgvMain.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMain_CellMouseLeave);
            // 
            // ColumnId
            // 
            this.ColumnId.DataPropertyName = "Id";
            this.ColumnId.FillWeight = 20F;
            this.ColumnId.HeaderText = "ID";
            this.ColumnId.Name = "ColumnId";
            this.ColumnId.ReadOnly = true;
            this.ColumnId.Visible = false;
            // 
            // ColumnOrderNum
            // 
            this.ColumnOrderNum.DataPropertyName = "OrderNum";
            this.ColumnOrderNum.FillWeight = 20F;
            this.ColumnOrderNum.HeaderText = "序号";
            this.ColumnOrderNum.Name = "ColumnOrderNum";
            this.ColumnOrderNum.ReadOnly = true;
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "Name";
            this.ColumnName.FillWeight = 60F;
            this.ColumnName.HeaderText = "名称";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            // 
            // ColumnUrl
            // 
            this.ColumnUrl.DataPropertyName = "Url";
            this.ColumnUrl.FillWeight = 70F;
            this.ColumnUrl.HeaderText = "播放地址";
            this.ColumnUrl.Name = "ColumnUrl";
            this.ColumnUrl.ReadOnly = true;
            // 
            // ColumnFace
            // 
            this.ColumnFace.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnFace.DataPropertyName = "Face";
            this.ColumnFace.FillWeight = 30F;
            this.ColumnFace.HeaderText = "封面图";
            this.ColumnFace.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.ColumnFace.Name = "ColumnFace";
            this.ColumnFace.ReadOnly = true;
            // 
            // gpTypes
            // 
            this.gpTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpTypes.Controls.Add(this.flowLayoutpaneltypes);
            this.gpTypes.Location = new System.Drawing.Point(3, 130);
            this.gpTypes.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gpTypes.Name = "gpTypes";
            this.gpTypes.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gpTypes.Size = new System.Drawing.Size(792, 103);
            this.gpTypes.TabIndex = 8;
            this.gpTypes.TabStop = false;
            this.gpTypes.Text = "分类";
            // 
            // flowLayoutpaneltypes
            // 
            this.flowLayoutpaneltypes.AutoScroll = true;
            this.flowLayoutpaneltypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutpaneltypes.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.flowLayoutpaneltypes.Location = new System.Drawing.Point(2, 21);
            this.flowLayoutpaneltypes.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.flowLayoutpaneltypes.Name = "flowLayoutpaneltypes";
            this.flowLayoutpaneltypes.Size = new System.Drawing.Size(788, 79);
            this.flowLayoutpaneltypes.TabIndex = 0;
            // 
            // FrmSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 650);
            this.Controls.Add(this.gpTypes);
            this.Controls.Add(this.dgvMain);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximumSize = new System.Drawing.Size(1536, 934);
            this.Name = "FrmSearch";
            this.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.Text = "资源检索";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).EndInit();
            this.gpTypes.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtkeyword;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsTipText;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbtnAdd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripLabel tslblMsg;
        private System.Windows.Forms.DataGridView dgvMain;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnOrderNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUrl;
        private System.Windows.Forms.DataGridViewImageColumn ColumnFace;
        private System.Windows.Forms.GroupBox gpTypes;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutpaneltypes;
        private System.Windows.Forms.ToolStripButton tsbtnCopyUrl;
        private System.Windows.Forms.ToolStripButton tsbtnNextPage;
        private System.Windows.Forms.ToolStripButton tsbtnSites;
        private System.Windows.Forms.ToolStripButton tsbtnPotPlayer;
        private System.Windows.Forms.ToolStripButton btnDownload;
    }
}