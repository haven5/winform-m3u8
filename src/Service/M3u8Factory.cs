﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormTsService.Models;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Collections;

namespace WinFormTsService.Service {
    /// <summary>
    /// 文件下载工厂
    /// </summary>
    public class M3u8Factory {

        static M3u8Factory _instance = null;
        List<TsFileDownload> Tasks = new List<TsFileDownload>(10);
        Boolean download = true;
        #region 单例模式
        /// <summary>
        /// 单例模式  实例对象
        /// </summary>
        public static M3u8Factory Instance {
            get {
                if (_instance == null) {
                    _instance = new M3u8Factory();
                }
                return _instance;
            }
        }
        #endregion
        /// <summary>
        /// 消费者数量
        /// </summary>
        public int TaskCount { get; set; } = 3;

        BlockingCollection<M3u8FileDto> M3u8Files = new BlockingCollection<M3u8FileDto>();
        /// <summary>
        /// 文件下载状态变更
        /// </summary>
        public Action<TsFileInfo, Exception> TsChange = null;
        public Action<M3u8FileDto, Exception> FileChange = null;

        #region 0.2 构造初始化
        private M3u8Factory() {
            for (int i = 0; i < TaskCount; i++) {
                Task.Run(() => Start());
            }
        }
        #endregion

        #region 1.0 新增任务
        /// <summary>
        /// 必须是等待中的任务
        /// </summary>
        /// <param name="dto"></param>
        public void Add(List<M3u8FileDto> rows) {
            foreach (M3u8FileDto item in rows) {
                if (item.Status == FileStatus.Wait
                    || item.Status == FileStatus.Abort
                    || item.Status == FileStatus.Convert
                    || item.Status == FileStatus.Error) {

                    if (!M3u8Files.TryAdd(item)) {
                        Console.WriteLine("Add Err");
                    }
                }
            }
        }
        #endregion

        #region 2.0 开始下载
        public void Start() {
            M3u8FileDto dto = null;
            while (download) {
                if (!M3u8Files.TryTake(out dto)) {
                    //Console.WriteLine("M3u8Files=>{0}", M3u8Files.Count);
                    Thread.Sleep(100);
                    continue;
                }
                TsFileDownload tsFile = new TsFileDownload(dto);
                Tasks.Add(tsFile);
                if (dto.Status != FileStatus.Convert) {
                    dto.Status = FileStatus.Download;
                }
                this.FileChange.Invoke(dto, null);
                tsFile.TsChange = this.TsChange;
                tsFile.FileChange = this.FileChange;
                tsFile.Start();
            }
        }
        #endregion


        #region 3.0 使用ffmpeg合并文件 +void MergeTsFiles(List<TsFileInfo> list)
        /// <summary>
        /// 使用ffmpeg合并文件
        /// </summary>
        /// <param name="list"></param>
        public static void MergeTsFiles(List<TsFileInfo> list, string folder, String outFileName) {
            /*
             .\ffmpeg.exe -i  "concat:9OTsTIRz.ts|13EuC10e.ts|Dbdmr721.ts|V2AalZy3.ts" -c copy ../video.mp4
            格式转为MP4
            .\ffmpeg.exe  -i ../video.mp4 -c:v libx264 -c:a aac -strict -2 ../output.mp4
             */
            List<String> files = new List<string>();
            foreach (TsFileInfo item in list) {
                files.Add(String.Format("./{0}/{1}", folder, item.FileName));
            }
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo();
            process.StartInfo.FileName = @"./ffmpeg.exe";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.Arguments = $"-i \"concat:{String.Join("|", files)}\" -c copy ./{folder}/{outFileName} ";
            process.Start();
            String output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            if (process.ExitCode != 0) {
                throw new Exception("ExitCode is " + process.ExitCode);
            }
            process.Dispose();
        }
        #endregion

        #region 4.0 将文件转为MP4格式 +void MergeMp4Files(string source, String outFileName)
        /// <summary>
        /// 将文件转为MP4格式
        /// </summary>
        /// <param name="list"></param>
        public static void ToMp4File(string source, String outFileName) {
            /*
             .\ffmpeg.exe -i  "concat:9OTsTIRz.ts|13EuC10e.ts|Dbdmr721.ts|V2AalZy3.ts" -c copy ../video.mp4
            格式转为MP4
            .\ffmpeg.exe  -i ../video.mp4 -c:v libx264 -c:a aac -strict -2 ../output.mp4

            .\ffmpeg.exe  -i ../video.mp4  -vcodec copy -acodec copy  ../output.mp4

             */
            //String folder = Path.GetDirectoryName(source);
            //outFileName = $"{folder}/{outFileName}";
            //if (File.Exists(outFileName)) {
            //    File.Delete(outFileName);
            //}
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo();
            process.OutputDataReceived += Process_OutputDataReceived;
            process.ErrorDataReceived += Process_OutputDataReceived;
            process.StartInfo.FileName = @"./ffmpeg.exe";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.RedirectStandardInput = true;
            //process.StartInfo.Arguments = $"-i  {source}  -c:v libx264 -c:a aac -strict -2  {outFileName} -y";
            process.StartInfo.Arguments = $"-i \"{source}\" -loglevel quiet -vcodec copy -acodec copy -y \"{outFileName}\"";
            process.Start();
            process.WaitForExit();
            if (process.ExitCode != 0) {
                throw new Exception("ExitCode is " + process.ExitCode);
            }
            process.Dispose();
        }


        private static void Process_OutputDataReceived(object sender, DataReceivedEventArgs e) {
            String msg = e.Data;
            Console.WriteLine(msg);
        }
        #endregion

        #region 清理资源
        public void Dispose() {
            download = false;
            // 通知BlockingCollection，生产者不再产生数据  
            M3u8Files.Dispose(); // 清理资源  
            foreach (TsFileDownload item in Tasks) {
                item.Dispose();
            }
        }
        #endregion




    }
}
