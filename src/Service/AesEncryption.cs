﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WinFormTsService.Service {
    /// <summary>
    /// AES 加密
    /// </summary>
    [Description("AES-128")]
    public class AesEncryption : TsEncryption {

        private byte[] key = null;
        private byte[] iv = null;
        public AesEncryption() {

        }

        #region 1.0 创建对象所需参数 +void Create(params object[] args)
        /// <summary>
        /// String _key, String _iv
        /// </summary>
        /// <param name="dic"></param>
        public override void Create(Dictionary<String, String> dic) {
            String valkey = dic["key"] ;
            if (valkey.StartsWith("0x")) {
                key = M3u8FileConvert.StringToHex(valkey); 
            } else {
                key = Encoding.UTF8.GetBytes(valkey);
            }
            if (!dic.ContainsKey("iv")) {
                iv = Encoding.UTF8.GetBytes(valkey);
                return;
            }
            String valiv = dic["iv"];
            if (valiv.StartsWith("0x")) {
                iv = M3u8FileConvert.StringToHex(valiv);

            } else {
                iv = Encoding.UTF8.GetBytes(valiv);
            } 
        }
        #endregion



        #region 1.0 解密文件内容 +StreamReader Decrypt(byte[] buffer) 
        /// <summary>
        /// 解密文件内容
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public override Stream Decrypt(byte[] buffer) {
            Aes aes = Aes.Create();
            ICryptoTransform transform = aes.CreateDecryptor(key, iv);
            MemoryStream memory = new MemoryStream(buffer);
            CryptoStream crypto = new CryptoStream(memory, transform, CryptoStreamMode.Read);
            return crypto;
        }
        #endregion

    }
}
