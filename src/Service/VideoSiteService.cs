﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.IO;
using System.Xml;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WinFormTsService.Models;
using System.Reflection;
using System.ComponentModel;
using System.Drawing;
using Newtonsoft.Json.Linq;

namespace WinFormTsService.Service
{
    /// <summary>
    /// 站点资源搜索服务 
    /// </summary>
    public class VideoSiteService
    {
        //资源检索

        /*
         * 
            https://zy.catni.cn/easyconfig.json

            https://www.39kan.com/api.php/provide/vod/

            https://ghproxy.net/https://raw.githubusercontent.com/ZTHA000/tvbox/main/thzx1.json

            https://video16.bpzy1.com/video/20220221/7199f912b15061dc0fca6bcc7831b818/index.m3u8

            /inc/sapi.php?ac=videolist&t=12&pg=1&f=
            /inc/sapi.php?ac=videolist&wd=H
            /inc/sapi.php?ac=videolist&ids=56817
            https://www.39kan.com/api.php/provide/vod/

            https://www.msnii.com/api/xml.php?ids=245804
         */
        /// <summary>
        /// 使用的站点
        /// </summary>
        public String Host { get; set; } = "https://collect.wolongzyw.com/api.php/provide/vod/";
        /// <summary>
        /// 站点主键
        /// </summary>
        public String SiteId { get; set; } 
        /// <summary>
        /// 视频分类
        /// </summary>
        public String VideoType { get; set; } 
        public const String FILE_Site = "site.json";
        /// <summary>
        /// 图片加载成功
        /// </summary>
        public Action<XmlVideo> ImageChange { get; set; }
        /// <summary>
        /// 视频条目加载成功
        /// </summary>
        public Action<PageList>  PageChange { get; set; }
        /// <summary>
        /// 分类数据加载成功
        /// </summary>
        public Action<List<VideoClass>> TypeChange = null;
        public Action<String > ShowMessage = null;
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; } = 1;
         public VideoSiteService() { 
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        }

        #region 1.0 显示消息 +void ShowInfoMsg(String msg, params object[] args)
        /// <summary>
        /// 显示消息
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="args"></param>
        private void ShowInfoMsg(String msg, params object[] args) {
            String message = String.Format(msg,args);
            ShowMessage?.Invoke(message);
        } 
        #endregion

        #region 2.0 获取视频文件列表 + void GetList(int  page= 1,String wd = "", String type = "")
        /// <summary> 
        /// 获取视频文件列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="wd"></param>
        /// <param name="type"></param>
        public PageList GetList(String wd = "") {
            // /inc/sapi.php?ac=videolist&t=12&pg=1&f=
            StringBuilder builder = new StringBuilder(200);
            builder.Append(Host);
            builder.AppendFormat("?ac=videolist");
            builder.AppendFormat("&pg={0}", PageIndex);
            if (!String.IsNullOrEmpty(wd)) {
                builder.AppendFormat("&wd={0}", wd);
            }
            if (!String.IsNullOrEmpty(VideoType)) {
                builder.AppendFormat("&t={0}", VideoType);
            }
             
            try { 
                String contents = GetHttpString(builder.ToString());
                //Console.WriteLine(contents);
                PageList pageList = this.ToPageList(contents);
                PageChange?.Invoke(pageList);
                Task.Run(() => { LoadImageChange(pageList); }); 
                return pageList;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                ShowInfoMsg("视频列表加载失败！=>{0}", ex.Message);
            }
            return null;

        }
        #endregion

        #region 2.1 获取Http响应内容 +String GetHttpString(String url)
        /// <summary>
        /// 获取Http响应内容
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static String GetHttpString(String url) { 
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
            Stream stream = request.GetResponse().GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            String contents = reader.ReadToEndAsync().Result;
            reader.Close();
            reader.Dispose();
            stream.Close();
            stream.Dispose();
            return contents;
        } 
        #endregion

        #region 3.0 加载分类 +XmlPageList GetClass() 
        /// <summary>
        /// 加载分类
        /// </summary> 
        public PageList GetClass() {

            // /inc/sapi.php?ac=videolist&t=12&pg=1&f=
            StringBuilder builder = new StringBuilder(200); 
            builder.Append(Host); 
            try {

                String contents = GetHttpString(builder.ToString());
                //Console.WriteLine(contents);
                PageList pageList = this.ToPageList(contents);
                TypeChange?.Invoke(pageList.Types);
                return pageList;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                ShowInfoMsg("视频分类加载失败！=>{0}", ex.Message);
            }

            return null;

        }
        #endregion

        #region 4.0 Xml转为实体对象 +XmlPageList ToPageList(String contents)
        /// <summary>
        /// Xml转为实体对象
        /// </summary>
        /// <param name="contents"></param>
        /// <returns></returns>
        public PageList ToPageList(String contents) {
            if (String.IsNullOrEmpty(contents)) {
                return null;
            }
            PageList page =  new PageList();
            contents = contents.Trim();
            if (contents.StartsWith("{")) {
                page = ToJsonPageList(contents);
            } else if (contents.StartsWith("<?xml")) {
                page = ToXmlPageList(contents);
            } else {
                ShowInfoMsg("未知数据格式！=>{0}", contents);
            }
            return page;
        }

        #endregion

        #region 4.1 解析json资源 + PageList ToJsonPageList(String contents)
        /// <summary>
        /// 解析json资源
        /// </summary>
        /// <param name="contents"></param>
        /// <returns></returns>
        public PageList ToJsonPageList(String contents) {
            PageList page = new PageList();
            dynamic data = JsonConvert.DeserializeObject(contents);
            page.Page = Convert.ToInt32(data.page);
            page.TotalCount = Convert.ToInt32(data.total);
            page.PageSize = Convert.ToInt32(data.limit);
            JArray list = data.list;
            if (list == null) {
                return page;
            }


            foreach (JObject item in list) {
                //Console.WriteLine(item.ToString());
                XmlVideo video = GetVideoInfo(item);
                page.Data.Add(video);
            }
            JArray types = data.@class;

            if(types == null) {
                return page;
            }
            foreach (JObject item in types) {
                VideoClass info = new VideoClass();
                info.Id =  item.GetValue("type_id").ToString();
                info.Name = item.GetValue("type_name").ToString();
                page.Types.Add(info); 

            }
            return page;
        }
        #endregion

        #region 4.2 解析Xml文件资源 + XmlPageList ToXmlPageList(String contents)
        /// <summary>
        /// 解析Xml文件资源
        /// </summary>
        /// <param name="contents"></param>
        /// <returns></returns>
        public PageList ToXmlPageList(String contents) {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(contents);
            XmlNode node = xml.SelectSingleNode("rss");
            XmlNode nodeList = node.SelectSingleNode("list");
            PageList xmlPage = new PageList();
            // page="1" pagecount="967" pagesize="20" recordcount="19340"
            XmlAttribute nodePage = nodeList.Attributes["page"];
            xmlPage.Page = int.Parse(nodePage.Value);
            XmlAttribute nodePagesize = nodeList.Attributes["pagesize"];
            xmlPage.PageSize = int.Parse(nodePagesize.Value);
            XmlAttribute nodeRecordcount = nodeList.Attributes["recordcount"];
            xmlPage.TotalCount = long.Parse(nodeRecordcount.Value);
            XmlNodeList list = nodeList.ChildNodes;
            foreach (XmlNode item in list) {
                XmlVideo video = GetVideoInfo(item);
                xmlPage.Data.Add(video);
            }


            XmlNode nodeClass = node.SelectSingleNode("class");
            if (nodeClass != null) {
                foreach (XmlNode item in nodeClass.ChildNodes) {
                    VideoClass t = ToTypesInfo(item);
                    xmlPage.Types.Add(t);
                }
            }

            return xmlPage;
        }
        #endregion

        #region 5.0 获取Xml节点中的视频文件信息 +XmlVideo GetVideoInfo(XmlNode node)
        /// <summary>
        /// 获取Xml节点中的视频文件信息
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public XmlVideo GetVideoInfo(XmlNode node) {
            XmlVideo video = new XmlVideo();
            Type t = typeof(XmlVideo);
            PropertyInfo[] props = t.GetProperties();
            foreach (PropertyInfo item in props) {
                DescriptionAttribute attribute = item.GetCustomAttribute<DescriptionAttribute>();
                if (attribute == null) {
                    continue;
                }
                XmlNode xmlNode = node.SelectSingleNode(attribute.Description);
                if (xmlNode == null) {
                    continue;
                }
                String val = xmlNode.InnerText;
                item.SetValue(video, val);
            }
            XmlNode dlNode = node.SelectSingleNode("dl");

            if (dlNode == null) {

                return video;
            }
            if (dlNode.ChildNodes.Count == 1) {
                XmlNode nodePlay = dlNode.ChildNodes[0];
                video.PlayList = ToPlayList1(dlNode.ChildNodes, video);
                return video;
            }
            int index = 1;
            foreach (XmlNode item in dlNode.ChildNodes) {
                XmlPlay play = new XmlPlay();
                play.Index = index++;
                play.Name = $"第{index.ToString("D4")}集";
                play.Url = item.InnerText;
                video.PlayList.Add(play);
            }
            return video;
        }
        #endregion

        #region 5.01 解析节点中的播放列表
        /// <summary>
        /// 解析节点中的播放列表
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private List<XmlPlay> ToPlayList1(XmlNodeList nodes, XmlVideo video) { 
            List<XmlPlay> list = new List<XmlPlay>();
            int index = 1;
            foreach (XmlNode item in nodes) { 
                XmlPlay play = new XmlPlay();
                play.Index = index;
                play.Name = video.Name;
                play.Url = item.InnerText;
                String playerUrl = item.InnerText;
                if (item.InnerText.Contains("$")) {
                    String[] urls = playerUrl.Split('$');
                    for (int i = 0; i < urls.Length; i += 2) {
                        play = new XmlPlay();
                        play.Index = index;
                        play.Name = video.Name;
                        play.Url = urls[i+1];
                        list.Add(play);
                    }
                } else {
                    list.Add(play);
                }

                index++;
            } 
            return list;
        }
        #endregion

        #region 5.02 解析节点中的播放列表
        /// <summary>
        /// 解析节点中的播放列表
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private List<XmlPlay> ToPlayList2(XmlNodeList nodes, XmlVideo video) {
            List<XmlPlay> list = new List<XmlPlay>();
            int index = 1;
            foreach (XmlNode item in nodes) {
                XmlPlay play = new XmlPlay();
                play.Index = index;
                play.Name = video.Name;
                play.Url = item.InnerText;
                String playerUrl = item.InnerText;
                if (item.InnerText.Contains("$")) {
                    String[] urls = playerUrl.Split('$');
                    for (int i = 0; i < urls.Length; i += 2) {
                        play = new XmlPlay();
                        play.Index = index;
                        play.Name = urls[i];
                        play.Url = urls[i + 1];
                        list.Add(play);
                    }
                } else {
                    list.Add(play);
                }

                index++;
            }
            return list;
        }
        #endregion

        #region 6.0 获取Xml节点中的视频分类信息 +XmlVideo ToTypesInfo(XmlNode node)
        /// <summary>
        /// 获取Xml节点中的视频分类信息
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public VideoClass ToTypesInfo(XmlNode node) {
            VideoClass t = new VideoClass();
            t.Id = node.Attributes["id"].Value;
            t.Name = node.InnerText;
            return t;
        }
        #endregion

        #region 7.0 获取JSON中的视频文件信息 +XmlVideo GetVideoInfo(XmlNode node)
        /// <summary>
        /// 获取JSON中的视频文件信息
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public XmlVideo GetVideoInfo(JObject obj) {
            XmlVideo video = new XmlVideo();
            JToken token = null;
            Type t = typeof(XmlVideo);

            Dictionary<String, String> dic = new Dictionary<string, string>() {
                {  "vod_id",nameof(XmlVideo.Id)},
                {  "type_id",nameof(XmlVideo.TypeId)},
                {  "type_name",nameof(XmlVideo.Type)},
                {  "vod_director",nameof(XmlVideo.Director)},
                {  "vod_area",nameof(XmlVideo.Area)},
                {  "vod_author",nameof(XmlVideo.Actor)},
                {  "vod_content",nameof(XmlVideo.Desc)},
                {  "vod_lang",nameof(XmlVideo.Lang)},
                {  "vod_name",nameof(XmlVideo.Name)},
                {  "vod_pic",nameof(XmlVideo.FaceUrl)},
                {  "vod_year",nameof(XmlVideo.Year)},
                {  "vod_blurb",nameof(XmlVideo.Note)},
                {  "vod_time",nameof(XmlVideo.Last)}
            };
            foreach (KeyValuePair<String, String> item in dic) {
                PropertyInfo p = t.GetProperty(item.Value);
                if (p == null) {
                    continue;
                }
                if (!obj.TryGetValue(item.Key, out token)) {
                    continue;
                }
                p.SetValue(video, token.ToString());
            }
            if (!obj.TryGetValue("vod_play_url", out token)) {
                return video;
            }
            // #  => $
            String urls = token.ToString();
            String[] sourceRows = urls.Split(new String[] { "$$$" },StringSplitOptions.RemoveEmptyEntries);
            if (sourceRows.Length > 1) {
                urls = sourceRows[0];
            }
            String[] rows = urls.Split('#');
            if (rows.Length == 1) {
                String[] playUrls = urls.Split('$');
                String url = urls;
                if (playUrls.Length > 1) {
                    url = playUrls[1];
                }
                video.PlayList.Add(new XmlPlay() { Index = 1, Name = video.Name, Url = url });
                return video;
            }
            int index = 1;

            foreach (String item in rows) {
                String[] playUrls = item.Split('$');
                String url = item;
                String name = video.Name;

                if (playUrls.Length > 1) {
                    //存在名称
                    name = playUrls[0];
                    url = playUrls[1];
                } else {
                    name = $"第{index.ToString("D4")}集";
                }
                XmlPlay play = new XmlPlay();
                play.Index = index++;
                play.Name = name;
                play.Url = url;
                video.PlayList.Add(play);
            }
            return video;
        }
        #endregion

        #region 8.0 获取Json中的视频分类信息 +XmlVideo ToTypesInfo(XmlNode node)
        /// <summary>
        /// 获取Json中的视频分类信息
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public VideoClass ToTypesInfo2(XmlNode node) {
            VideoClass t = new VideoClass();
            t.Id = node.Attributes["id"].Value;
            t.Name = node.InnerText;
            return t;
        }
        #endregion

        #region 9.0 获取图像流数据 +static Image LoadImage(String url)
        /// <summary>
        /// 获取图像流数据
        /// </summary>
        /// <param name="url"></param>
        public static Image LoadImage(String url) {
           HttpWebRequest   request =(HttpWebRequest) WebRequest.Create(url);
            request.Timeout = 3000; 
            try {
                request.ServerCertificateValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
                Stream stream = request.GetResponse().GetResponseStream();
                 
                return Image.FromStream(stream);

            } catch (Exception ex) {
                Console.WriteLine("LoadImage Err=>{0}",ex.Message);
            } finally {
                
            }
            return null;
        }
        #endregion

        #region 10.0 加载图像流数据并通知界面更新 +static Image LoadImage(String url)
        /// <summary>
        /// 加载图像流数据并通知界面更新
        /// </summary>
        /// <param name="pageList"></param>
        public void LoadImageChange(PageList pageList) {
            foreach (XmlVideo item in pageList.Data) {
                if (String.IsNullOrEmpty(item.FaceUrl)) {
                    continue;
                }
                item.Face = LoadImage(item.FaceUrl);
                ImageChange?.Invoke(item);
            }
        }
        #endregion

        #region 11.0 加载地址中的站点信息 +  List<SiteInfo> LoadSiteInfo(String url)
        /// <summary> 
        /// 加载地址中的站点信息
        /// </summary>
        /// <param name="url"></param> 
        public List<SiteInfo> LoadSiteInfo(String url) {
            List<SiteInfo> sites = new List<SiteInfo>();
            WebClient client = new WebClient();
            try {
                byte[] buffer = client.DownloadData(url);
                String contents = Encoding.UTF8.GetString(buffer);
                dynamic data = JsonConvert.DeserializeObject(contents);
                dynamic objData = data.sites;
                if (objData == null) {
                    return sites;
                }
                JArray list = objData.data;
                foreach (JObject item in list) {
                    SiteInfo info = new SiteInfo();
                    object api = item.GetValue("api");
                    if (api == null) {
                        continue;
                    }
                    info.Id = item.GetValue("id").ToString();
                    info.Name = item.GetValue("name").ToString();
                    info.Key = item.GetValue("key").ToString();
                    info.Api = api.ToString();
                    sites.Add(info);
                }
                String sitesContents = JsonConvert.SerializeObject(sites);
                File.WriteAllText(FILE_Site, sitesContents, Encoding.UTF8);
                return sites;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return sites;

        }
        #endregion

        #region 12.0 加载本地文件中的站点信息 +  List<SiteInfo> LoadLocalSiteInfo( ) 
        /// <summary>
        /// 加载本地文件中的站点信息
        /// </summary>
        /// <returns></returns>
        public List<SiteInfo> LoadLocalSiteInfo() {
            List<SiteInfo> sites = new List<SiteInfo>();
            try {
                if (!File.Exists(FILE_Site)) {
                    return sites;
                }

                String sitesContents = File.ReadAllText(FILE_Site, Encoding.UTF8);
                sites = JsonConvert.DeserializeObject<List<SiteInfo>>(sitesContents);

                return sites;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return sites;

        }
        #endregion

    }
}
