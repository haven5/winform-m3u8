﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WinFormTsService.Models;

namespace WinFormTsService.Service {


    public class M3u8FileConvert {

        public const String SettingsFile = "Settings.json";
        /// <summary>
        /// 文件下载地址
        /// </summary>
        public const String FOLDER_DOWNLOAD = "download";
        /// <summary>
        /// 视频文件目录
        /// </summary>
        public const String FOLDER_VIDEO = "videos";
        /// <summary>
        /// 文件地址
        /// </summary>
        public const String FOLDER_MERGE = "MergeFiles";

        #region 1.0 转换文本为实体对象 +List<M3u8FileDto> Convert(String contents)
        /// <summary>
        /// 转换文本为实体对象
        /// </summary>
        /// <param name="contents"></param>
        /// <returns></returns>
        public static List<M3u8FileDto> M3u8Convert(String contents) {
            List<M3u8FileDto> list = new List<M3u8FileDto>(10);
            if (String.IsNullOrEmpty(contents)) {
                return list;
            }
            contents = contents.Replace("\r", string.Empty);
            String[] rows = contents.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            int rowIndex = 0;
            for (int i = 0; i < rows.Length -1; i++) {
                String item = rows[i];
                M3u8FileDto dto = new M3u8FileDto();
                dto.Status = FileStatus.Wait;
                if (item.StartsWith("http")) {
                    dto.Url = item;
                    dto.FileName = String.Format("第{0}集", (i + 1).ToString("D4"));
                    dto.Id = ++rowIndex;
                    dto.FileId = Md5String(item).Substring(0x00,10);
                    list.Add(dto);
                    continue;
                } else {
                    dto.FileName = item;
                    dto.Url = rows[i + 1];
                    dto.FileId = Md5String(dto.Url).Substring(0x00, 10);
                    i++;
                    dto.Id = ++rowIndex;
                    list.Add(dto);
                }
            }
            return list;
        }

        #endregion

        #region 2.0 MD5字符串加密 +String Md5String(String url)
        /// <summary>
        /// MD5字符串加密
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static String Md5String(String url) {
            byte[] buffer = Encoding.UTF8.GetBytes(url.Trim());
            byte[] lstHash = new MD5CryptoServiceProvider().ComputeHash(buffer);
            StringBuilder result = new StringBuilder(32);
            for (int i = 0; i < lstHash.Length; i++) {
                result.Append(lstHash[i].ToString("x2"));
            }
            return result.ToString();
        }
        #endregion

        #region 3.0 获取Url地址对应的Ts文件信息 
        /// <summary>
        /// 下载ts文件并自动合并
        /// </summary>
        /// <param name="url"></param>
        public static List<TsFileInfo> GetUrlTsFiles(M3u8FileDto dto) {
            List<TsFileInfo> list = new List<TsFileInfo>();
            WebClient client = new WebClient();
            try {
                client.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36");
                client.Headers.Add("Upgrade-Insecure-Requests", "1");
                client.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                String contents = client.DownloadString(dto.Url);
            
                if (String.IsNullOrEmpty(contents)) {
                    return list;
                }
                list =  DownloadTs(contents, dto);
                client.Dispose();
                return list;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                client.Dispose();
                return list;
            }
        }
        #endregion

        #region 4.0 解析TS文件信息 +List<TsFileInfo> DownloadTs(String contents, String host, List<TsFileInfo> list)
        /// <summary>
        /// 解析TS文件信息
        /// </summary>
        /// <param name="contents"></param>
        /// <param name="host"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public static List<TsFileInfo> DownloadTs(String contents, M3u8FileDto dto ) {

            List<TsFileInfo> list = new List<TsFileInfo>();
            String[] lines = contents.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            int rowIndex = 1;
            Uri source  = new Uri(dto.Url);
            for (int i = 0; i < lines.Length; i++) {
                String row = lines[i];
                if (row.StartsWith("#EXT-X-STREAM-INF")) {
                    //这是视频信息
                } else if (row.EndsWith(".ts")) {
                    String url = GetSourceUrl(row, source);
                    TsFileInfo file = new TsFileInfo();
                    file.FileId = Md5String(url);
                    file.FileName = String.Format("chunk-{0}.ts", rowIndex.ToString("D5"));
                    file.Status = FileStatus.Wait;
                    file.Index = rowIndex++;
                    file.Url = url;
                    //这是资源路径
                    list.Add(file);
                }else if (row.Contains(".m3u8")) {
                    String realUrl = String.Empty;
                    if (row.StartsWith("http")) {
                        realUrl = row;
                    }
                    if (row.StartsWith("/")) {
                        realUrl = String.Format("{0}://{1}{2}", source.Scheme, source.Host, row);
                    } else {
                        int index =  dto.Url.LastIndexOf("/");
                        realUrl = String.Format("{0}/{1}", dto.Url.Substring(0x00,index ), row);
                    }
                    dto.Url = realUrl;
                  return  GetUrlTsFiles(dto);
                }else if (row.StartsWith("#EXT-X-KEY")) {
                    //视频是加密的
                    GetEncrypt(row, dto); 
                }
            }
            return list;
        }
        #endregion

        #region 5.0 获取Ts文件的网络地址 +String GetSourceUrl(String path)
        /// <summary>
        /// 获取Ts文件的网络地址
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static String GetSourceUrl(String path, Uri host) {
            if (String.IsNullOrEmpty(path)) {
                return String.Empty;
            }
            String urlheader = host.Host;
            if (host.Scheme == "https" && host.Port== 443) {
                urlheader = String.Format("{0}://{1}", host.Scheme, host.Host);
            }else  if (host.Scheme == "http" && host.Port== 80) {
                urlheader = String.Format("{0}://{1}", host.Scheme, host.Host);
            }else   {
                urlheader = String.Format("{0}://{1}:{2}", host.Scheme, host.Host, host.Port);
            }

            if (path.StartsWith("/")) {
                String url = String.Format("{0}{1}", urlheader, path);
                return url;
            } else if (path.StartsWith("http")) {
                return path;
            } else {
                String folder = Path.GetDirectoryName(host.AbsolutePath);
                String url = String.Format("{0}{1}/{2}", urlheader,  folder, path);
                url = url.Replace("\\", "/");
                return url;
            }
        }

        #endregion

        #region 6.0 获取文件保存目录 +static String GetM3u8Folder(String fileId)
        /// <summary>
        /// 获取文件保存目录
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public static String GetM3u8Folder(String fileId) {
            String dir = Directory.GetCurrentDirectory();
            String folder = String.Format("{0}\\{1}\\{2}", dir, FOLDER_DOWNLOAD, fileId);
            if (!Directory.Exists(folder)) {
                Directory.CreateDirectory(folder);
            }
            return folder;
        }
        #endregion

        #region 7.0 保存配置信息到文件中 +static void SaveConfig(List<M3u8FileDto> m3u8Files)
        /// <summary>
        /// 保存配置信息到文件中
        /// </summary>
        /// <param name="m3u8Files"></param>
        public static void SaveConfig(List<M3u8FileDto> m3u8Files) {
            //int rowIndex = m3u8Files.Count;
            //foreach (M3u8FileDto item in m3u8Files) {
            //    item.Id = rowIndex--; 
            //}
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new  CamelCasePropertyNamesContractResolver();
            settings.Formatting = Formatting.Indented;
            String contents = JsonConvert.SerializeObject(m3u8Files, settings);
            File.WriteAllText(SettingsFile, contents, Encoding.UTF8);
        }
        #endregion

        #region 8.0 读取配置信息 +static List<M3u8FileDto> ReadConfig()
        /// <summary>
        /// 读取配置信息
        /// </summary>
        public static List<M3u8FileDto> ReadConfig() {
            List<M3u8FileDto> m3u8Files = new List<M3u8FileDto>();
            if (!File.Exists(SettingsFile)) {
                return m3u8Files;
            }
            String contents = File.ReadAllText(SettingsFile, Encoding.UTF8);
            m3u8Files = JsonConvert.DeserializeObject<List<M3u8FileDto>>(contents);
            return m3u8Files;
        }
        #endregion

        #region 9.0 去重重复的项 +List<M3u8FileDto> RemoveExists(List<M3u8FileDto> list, List<M3u8FileDto> items)
        /// <summary>
        /// 去重重复的项
        /// </summary>
        /// <param name="list"></param>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<M3u8FileDto> RemoveExists(List<M3u8FileDto> list, List<M3u8FileDto> items,out int rowCount ) {
            List<M3u8FileDto> rows = new List<M3u8FileDto>();
            int count = 0;
            foreach (M3u8FileDto item in items) {
                List<M3u8FileDto> models = list.FindAll(t => t.FileId == item.FileId);
                if (models.Count == 0) {
                    rows.Add(item);
                } else {
                    count++;
                }
            }
            rowCount = count;
            return rows;
        }
        #endregion

        #region 10.0 合并文件 +static String MergeFiles(List<String> files,String fileName ="")
        /// <summary>
        /// 合并文件
        /// </summary>
        /// <param name="files"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static String MergeFiles(List<String> files,String fileName ="") {

            String folder = M3u8FileConvert.GetM3u8Folder(FOLDER_MERGE);
            if (String.IsNullOrEmpty(fileName)) {
                fileName = String.Format("{0}\\{1}.mp4", folder, DateTime.Now.ToString("HHmmssff"));
            }
           
            FileStream stream = new FileStream(fileName, FileMode.Create);
            foreach (String file in files) { 
                if (!File.Exists(file)) {
                    continue;
                }
                byte[] buffer = File.ReadAllBytes(file);
                stream.Write(buffer, 0x00, buffer.Length);
            }
            stream.Close();
            stream.Dispose(); 
            return fileName;
        }
        #endregion

        #region 11.0 获取加密方式和Key +Dictionary<String, string> GetEncrypt(String line, M3u8FileDto dto) 
        /// <summary>
        /// 获取加密方式和Key
        /// </summary>
        /// <param name="line"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static Dictionary<String, string> GetEncrypt(String line, M3u8FileDto dto) {
            if (line.Contains("http")) {
                return GetHttpEncrypt(line, dto);
            }
            Dictionary<String, string> dic = new Dictionary<string, string>();
            String[] list = line.Split(':');
            if (list.Length < 2) {
                return dic;
            }
            list = list[1].Split(',');
            foreach (String item in list) {
                String[] kvs = item.Split('=');
                dic.Add(kvs[0].ToLower(), kvs[1]);
            }
            if (dic.ContainsKey("method")) {
                String method = dic["method"];
                dto.Method = method;
                dto.IsCrypt = true;
            } 
            if (dic.ContainsKey("uri")) {
                String uri = dic["uri"].Replace("\"",String.Empty);
                String addr = GetSourceUrl(uri,new Uri(dto.Url));
                WebClient client = new WebClient();
                try {
                    String key = client.DownloadString(addr);
                    dto.CryptKey.Add("key", key);
                    dic.Add("key", key);
                    Console.WriteLine(addr);
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                } finally {
                    client.Dispose();
                } 
            }
            if (dic.ContainsKey("iv")) {
                String iv = dic["iv"]; 
              dto.CryptKey.Add("iv", iv);
               
            }
            return dic;
        }


        public static Dictionary<String, string> GetHttpEncrypt(String line, M3u8FileDto dto) {
            //  #EXT-X-KEY:METHOD=AES-128,URI="https://t125.ezhouln.com/20241121/cneasuRt/1000kb/hls/key.key"

            Dictionary<String, string> dic = new Dictionary<string, string>();

            string keyUrl  = line.Substring(line.IndexOf(":") + 1);
            String[] list = keyUrl.Split(',');

            foreach (String item in list) {
                String[] kvs = item.Split('=');
                dic.Add(kvs[0].ToLower(), kvs[1]);
            }
            if (dic.ContainsKey("method")) {
                String method = dic["method"];
                dto.Method = method;
                dto.IsCrypt = true;
            }
            if (dic.ContainsKey("uri")) {
                String uri = dic["uri"].Replace("\"", String.Empty);
                String addr = GetSourceUrl(uri, new Uri(dto.Url));
                WebClient client = new WebClient();
                try {
                    String key = client.DownloadString(addr);
                    dto.CryptKey.Add("key", key);
                    dic.Add("key", key);
                    Console.WriteLine(addr);
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                } finally {
                    client.Dispose();
                }
            }
            if (dic.ContainsKey("iv")) {
                String iv = dic["iv"];
                dto.CryptKey.Add("iv", iv);

            }


            return dic;
        }
        #endregion

        #region 12.0 字符串转16进制 +byte[] HexToString(string hex)
        /// <summary>
        /// 字符串转16进制
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static byte[] StringToHex(string hex) {
            if (hex.Length % 2 != 0)
                throw new ArgumentException(String.Format("The binary key cannot have an odd number of digits, so it needs to be multiplied by 2."));
            if (hex.StartsWith("0x")) {
                hex = hex.Substring(2);
            }
            byte[] bytes = new byte[hex.Length / 2];
            for (int index = 0; index < bytes.Length; index++) {
                bytes[index] = Convert.ToByte(hex.Substring(index * 2, 2), 16);
            }
            return bytes;
        }
        #endregion

        #region 13.0 获取视频文件存放目录 +static String GetVideoFolder() 
        /// <summary>
        /// 获取视频文件存放目录
        /// </summary>
        /// <returns></returns>
        public static String GetVideoFolder() {
            String folder = String.Format("{0}\\{1}", Directory.GetCurrentDirectory(), FOLDER_VIDEO);
            if (!Directory.Exists(folder)) {
                Directory.CreateDirectory(folder);
            }
            return folder;
        } 
        #endregion
    }
}
