﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WinFormTsService.Service {
    /// <summary>
    /// ts文件解密
    /// </summary>
    public abstract class TsEncryption {
        /// <summary>
        /// 加密名称
        /// </summary>
        /// <returns></returns>

        public String Name { get; protected set; }

        public TsEncryption() {

        }

        #region 1.0 创建对象所需参数
        /// <summary>
        /// 创建对象所需参数
        /// </summary>
        /// <param name="args"></param>
        public virtual void Create(Dictionary<String, String> dic) {

        } 
        #endregion


        /// <summary>
        /// 解密字节数组
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public abstract Stream Decrypt(byte[] buffer);

        #region 1.0 使用反射获取ts文件的解密对象 +TsEncryption GetTsEncrypt(String method)
        /// <summary>
        /// 使用反射获取ts文件的解密对象
        /// </summary>
        /// <param name="method">解密方式 如：AES-128</param>
        /// <returns></returns>
        public static TsEncryption GetTsEncrypt(String method) {
            Type t = typeof(TsEncryption);
            List<Type> list = t.Assembly.GetTypes().Where(c => c.IsSubclassOf(t)).ToList();

            TsEncryption encryption = null;
            foreach (Type item in list) {
                DescriptionAttribute attr = item.GetCustomAttribute<DescriptionAttribute>();
                if (attr.Description == method) {
                    encryption = (TsEncryption)Activator.CreateInstance(item);
                    break;
                }
            }
            return encryption;
        } 
        #endregion

    }
}
