﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WinFormTsService.Models;

namespace WinFormTsService.Service {
    public class TsFileDownload {

        public M3u8FileDto Model = null;

        /// <summary>
        /// 文件下载状态变更
        /// </summary>
        public Action<TsFileInfo, Exception> TsChange = null;
        /// <summary>
        /// 项目文件下载状态变更
        /// </summary>
        public Action<M3u8FileDto, Exception> FileChange = null;

        WebClient client = new WebClient();

        public Boolean Downloading { get; set; }
        /// <summary>
        /// 是否释放资源
        /// </summary>
        Boolean IsDisposed { get; set; }

        TsEncryption Encryption = null;

        public TsFileDownload(M3u8FileDto model) {
            this.Model = model;
            if (!model.IsCrypt) {
                return;
            }
            Encryption = TsEncryption.GetTsEncrypt(model.Method);
            //文件是加密的需要加载解密器
            if (Encryption == null) {
                FileChange?.Invoke(model, new Exception("未实现对应文件解密器"));
            }
            Encryption.Create(model.CryptKey);
        }

        #region 1.0 开始下载所有Ts文件 +void Start() 
        /// <summary>
        /// 开始下载Ts文件
        /// </summary>
        public void Start() {
            int errCount = 0;
            IsDisposed = false;
            if (Model.TsFiles == null) {
                Model.TsFiles = M3u8FileConvert.GetUrlTsFiles(Model);
            }
            if (Model.Status == FileStatus.Convert) {
                SaveVideo();
                return;
            }
            foreach (TsFileInfo item in Model.TsFiles) {
                if (IsDisposed) {
                    Model.Status = FileStatus.Abort;
                    FileChange?.Invoke(Model, null);
                    return;
                }

                Downloading = true;
                item.Status = FileStatus.Download;
                TsChange?.Invoke(item, null);
                if (Download(item)) {
                    item.Status = FileStatus.Complete;
                    TsChange?.Invoke(item, null);
                } else {
                    item.Status = FileStatus.Error;
                    errCount++;
                }
                Downloading = false;
            }
            if (errCount == 0) {
                //全部下载完成且没有错误
                Model.Status = FileStatus.Convert;
                SaveVideo();
                FileChange?.Invoke(Model, null);
            }

        }

        #endregion

        #region 2.0 下载TS文件 +Boolean Download(TsFileInfo ts) 
        /// <summary>
        /// 下载TS文件
        /// </summary>
        /// <param name="ts"></param>
        /// <returns></returns>
        public Boolean Download(TsFileInfo ts) {
            try {
                String folder = M3u8FileConvert.GetM3u8Folder(Model.FileId);
                String fileName = String.Format("{0}\\{1}", folder, ts.FileName);
                if (File.Exists(fileName)) {
                    return true;
                }
                FileStream stream = new FileStream(fileName, FileMode.Create);
                byte[] buffer = client.DownloadData(ts.Url);
                if (Model.IsCrypt) {
                    Stream reader = Encryption.Decrypt(buffer);
                    reader.CopyTo(stream);
                    reader.Close();
                    reader.Dispose();
                } else {
                    stream.Write(buffer, 0x00, buffer.Length);
                }
                stream.Close();
                stream.Dispose();
                return true;
            } catch (Exception ex) {
                //Console.WriteLine(ex.Message);
                TsChange?.Invoke(ts, ex);
                FileChange?.Invoke(Model, ex);
                return false;
            }
        }
        #endregion

        #region 3.0 合并所有的TS文件为新的视频文件 +void SaveVideo()
        /// <summary>
        /// 合并所有的TS文件为新的视频文件
        /// </summary>
        public void SaveVideo() {
            String folder = M3u8FileConvert.GetM3u8Folder(Model.FileId);
            String fileName = String.Format("{0}\\{1}.tsv", folder, Model.FileId);
            String destFileName = String.Format("{0}\\{1}.mp4", folder, Model.FileId);
            if (File.Exists(fileName)) {
                File.Delete(fileName);
            }
            FileStream stream = new FileStream(fileName, FileMode.Create);
            foreach (TsFileInfo item in Model.TsFiles) {
                String file = String.Format("{0}\\{1}", folder, item.FileName);
                if (!File.Exists(file)) {
                    continue;
                }
                byte[] buffer = File.ReadAllBytes(file);
                stream.Write(buffer, 0x00, buffer.Length);
            }
            stream.Close();
            stream.Dispose();

            Model.Status = FileStatus.Convert;
            FileChange?.Invoke(Model, new Exception("转码中"));
            M3u8Factory.ToMp4File(fileName, destFileName);
            //移动文件到同一目录
            String mp4File = String.Format("{0}\\{1}.mp4", M3u8FileConvert.GetVideoFolder(), Model.FileName);
            File.Move(destFileName, mp4File);
            Model.Status = FileStatus.Complete;
            FileChange?.Invoke(Model, new Exception("已下载"));
            RemovetsFiles();
        }
        #endregion

        #region 4.0 合并完成之后清理中间文件
        /// <summary>
        /// 合并所有的TS文件为新的视频文件
        /// </summary>
        public void RemovetsFiles() {
            String folder = M3u8FileConvert.GetM3u8Folder(Model.FileId);
            String fileName = String.Format("{0}\\{1}.tsv", folder, Model.FileId);
            if (File.Exists(fileName)) {
                File.Delete(fileName);
            }
            foreach (TsFileInfo item in Model.TsFiles) {
                String file = String.Format("{0}\\{1}", folder, item.FileName);
                if (!File.Exists(file)) {
                    continue;
                }
                File.Delete(file);
            }
        }
        #endregion

        #region 9.0 清理释放资源 +void Dispose()
        /// <summary>
        /// 清理释放资源
        /// </summary>
        public void Dispose() {
            IsDisposed = true;
            Model.Status = FileStatus.Abort;
            //while (downloading) {
            //    Thread.Sleep(10);
            //}
            client.Dispose();
        }
        #endregion
    }
}
