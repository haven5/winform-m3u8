﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 
using WinFormTsService.Models;
using WinFormTsService.Service;

namespace WinFormTsService {
    public partial class FrmAddMultiple : Sunny.UI.UIForm {
        /// <summary>
        /// 新增的Ts文件列表
        /// </summary>
        public List<M3u8FileDto> M3u8Files = null;
        public FrmAddMultiple() {
            InitializeComponent();
            M3u8Files = new List<M3u8FileDto>(10);

        }
        private void btnOk_Click(object sender, EventArgs e) {
            String contents = txtInput.Text;
            if (String.IsNullOrEmpty(contents)) {
                MessageBox.Show("未输入数据！");
                return;
            }
            List<M3u8FileDto> list = M3u8FileConvert.M3u8Convert(contents);
            list.Reverse();
            M3u8Files.AddRange(list);
            //Console.WriteLine(list.Count);
            DialogResult = DialogResult.OK;

        }
    }
}
